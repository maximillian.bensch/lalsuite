//
// Copyright (C) 2018 Maximillian Bensch
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with with program; see the file COPYING. If not, write to the
// Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA
//

#include "ComputeFstat_internal.h"

// ---------- BEGIN: Resamp-specific timing model data ----------
typedef struct tagTimings_t
{
  REAL4 Total;		// total time spent in XLALComputeFstatResamp()
  REAL4 Bary;		// time spent (in this call) in barycentric resampling
  REAL4 Spin;		// time spent in spindown+frequency correction
  REAL4 FFT;		// time spent in FFT
  REAL4 Copy;		// time spent copying results from FFT to FabX
  REAL4 Norm;		// time spent normalizing the final Fa,Fb
  REAL4 Fab2F;		// time to compute Fstat from {Fa,Fb}
  REAL4 Mem;		// time to realloc and Memset-0 arrays
  REAL4 SumFabX;	// time to sum_X Fab^X
  BOOLEAN BufferRecomputed; // did we need to recompute the buffer this time?
} Timings_t;

// Resamp-specific timing model data
typedef struct tagFstatTimingResamp
{
  UINT4 NsampFFT0;		// original number of FFT samples (not rounded to power-of-two)
  UINT4 NsampFFT;		// actual number of FFT samples (rounded up to power-of-two if optArgs->resampFFTPowerOf2 == true)
  REAL4 Resolution;	// (internal) frequency resolution 'R' in natural units: df_internal = R / T_FFT\n

  REAL4 tau0_Fbin;      // timing coefficient for all contributions scaling with output frequency-bins
  REAL4 tau0_spin;      // timing coefficient for spindown-correction
  REAL4 tau0_FFT;       // timing coefficient for FFT-time
  REAL4 tau0_bary;      // timing coefficient for barycentering

  Timings_t Tau;

} FstatTimingResamp;
// ---------- END: Resamp-specific timing model data ----------

typedef struct
{
  UINT4 Dterms;						// Number of terms to use (on either side) in Windowed-Sinc interpolation kernel
  MultiCOMPLEX8TimeSeries  *multiTimeSeries_DET;	// input SFTs converted into a heterodyned timeseries
  // ----- buffering -----
  PulsarDopplerParams prev_doppler;			// buffering: previous phase-evolution ("doppler") parameters
  MultiAMCoeffs *multiAMcoef;				// buffered antenna-pattern functions
  MultiSSBtimes *multiSSBtimes;				// buffered SSB times, including *only* sky-position corrections, not binary
  MultiSSBtimes *multiBinaryTimes;			// buffered SRC times, including both sky- and binary corrections [to avoid re-allocating this]

  AntennaPatternMatrix Mmunu;				// combined multi-IFO antenna-pattern coefficients {A,B,C,E}
  AntennaPatternMatrix MmunuX[PULSAR_MAX_DETECTORS];	// per-IFO antenna-pattern coefficients {AX,BX,CX,EX}

  MultiCOMPLEX8TimeSeries *multiTimeSeries_SRC_a;	// multi-detector SRC-frame timeseries, multiplied by AM function a(t)
  MultiCOMPLEX8TimeSeries *multiTimeSeries_SRC_b;	// multi-detector SRC-frame timeseries, multiplied by AM function b(t)

  UINT4 numSamplesFFT;					// length of zero-padded SRC-frame timeseries (related to dFreq)
  UINT4 decimateFFT;					// output every n-th frequency bin, with n>1 iff (dFreq > 1/Tspan), and was internally decreased by n
  void *fftplan;					// FFT plan

  // ----- timing -----
  BOOLEAN collectTiming;				// flag whether or not to collect timing information
  FstatTimingGeneric timingGeneric;			// measured (generic) F-statistic timing values
  FstatTimingResamp  timingResamp;			// measured Resamp-specific timing model data

  void *MultiTimeStamps_GPU;
  FstatMethodType FstatMethod;

} ResampMethodData;


int XLALBarycentricResampleMultiCOMPLEX8TimeSeries ( ResampMethodData *resamp, const PulsarDopplerParams *thisPoint, const FstatCommon *common );
