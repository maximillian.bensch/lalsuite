/*
 * Copyright (C) 2018 Maximillian Bensch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

/*---------- INCLUDES ----------*/
#include <lal/OpenCLutils.h>

/*---------- internal types ----------*/
/** struct for saving information over an OpenCL-capable device */
typedef struct
{
  cl_device_id id;  /**< device id */
  CHAR *deviceName; /**< device name */
  CHAR *deviceVendor; /**< Name of the Vendor */
  REAL4 deviceVersion; /**< supported OpenCL version of the device */
  cl_device_type type; /**< CL_DEVICE_TYPE */
  cl_ulong globalMem; /**< CL_DEVICE_GLOBAL_MEM_SIZE */
  cl_ulong doubleAvailable; /**< will save CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, if this is '0', double is not supported */
  cl_uint vendorID; /**< Vendor ID of device */
} CLDEVICE;

/** Vector of type CLDEVICE */
typedef struct
{
  CLDEVICE **data; /**< array of CLDEVICE pointers */
  UINT4 length;    /**< Number of CLDEVICEs */
} CLDEVICEVector;

/** struct for saving information over an OpenCL-capable platform */
typedef struct
{
  cl_platform_id platform; /**< saves the platform id */
  CLDEVICEVector *devices;  /**< Vector of all devices of the platform */
  CHAR *platformName;  /**< saves the platform name */
  CHAR *platformVendor;  /**< saves the Name of the Vendor */
  REAL4 platformVersion;  /**< saves the OpenCL version of the device */
} CLPLATFORM;

/** Vector of type CLPLATFORM */
typedef struct
{
  CLPLATFORM **data;  /**< array of CLPLATFORM pointers */
  UINT4 length;       /**< Number of CLPLATFORMs */
} CLPLATFORMVector;

/*---------- Global variables ----------*/
/** OpenCL_OBJ which will be used for all OpenCL functions */
static OpenCL_OBJ openclObj;

/** common header for all kernels */
static const char headerOpenCL[] =
  "typedef float2 COMPLEX8;\n"
  "typedef float REAL4;\n"
  "typedef double REAL8;\n"
  "typedef uint UINT4;\n"
  "typedef int INT4;\n"
  "typedef long INT8;\n"
  "typedef int2 LIGOTimeGPS;\n"
  "COMPLEX8 local_cmulf ( COMPLEX8 in1, COMPLEX8 in2 )\n"
  "{\n"
  "    return (COMPLEX8) ( (in1).x*(in2).x - (in1).y*(in2).y , (in1).x*(in2).y +(in1).y*(in2).x );\n"
  "}\n";

/*---------- internal prototypes ----------*/
/**
 * We don't need any WorkGroup, but according to OpenCL specs:
 * - if OPENCL <= 1.2 WorkGroup is chosen as Size is a multiple of WorkGroup
 * - if OPENCL >= 2.0 The driver will perform a final call with remaining items to be processed
 *
 **/
static inline int bestWorkSize ( UINT4 size ) {
  // use CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE to find the best workgroup size
  size_t multi = openclObj.preferredWorkGroupSizeMultiple;
  return ( size + ( multi - 1 ) ) & ( -multi );
}

static char* XLALGenerateGeneralKernels ( void );
static CLDEVICE* XLALSelectCLDEVICE ( void );
static CLPLATFORMVector* XLALGetOpenCLPLATFORMVector ( void );
static void XLALDestroyCLPLATFORMVector ( CLPLATFORMVector *vec );
static void XLALDestroyCLPLATFORM ( CLPLATFORM *platform );
static void XLALDestroyCLDEVICEVector ( CLDEVICEVector *vec );
static void XLALDestroyCLDEVICE ( CLDEVICE *device );

/*==================== FUNCTION DEFINITIONS ====================*/

#define DEFINE_CLMEMVECTOR_MEMSET_API(TYPE)                                   \
int XLALMemsetCLMEMVector##TYPE ( CLMEMVector *in, const TYPE scalar, BOOLEAN last ) \
{\
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );\
  XLAL_CHECK ( in != NULL, XLAL_EINVAL );\
  cl_int err;\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_Memset##TYPE , 0, sizeof(in->data),   (void*) &(in->data)         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_Memset##TYPE , 1, sizeof(scalar),     (const void*) &(scalar)     ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_Memset##TYPE , 2, sizeof(in->length), (const void*) &(in->length) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
                                                                                 \
  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &openclObj.kernel.kernel_Memset##TYPE , in->length, last ) == XLAL_SUCCESS, XLAL_EFUNC );\
                                                                                 \
  return XLAL_SUCCESS;\
} /* XLALMemsetCLMEMVector\<TYPE\>() */            \

#define DEFINE_CLMEMVECTOR_V2VV_API(TYPE,OP)                                   \
int XLAL##OP##CLMEMVector##TYPE ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 length, BOOLEAN last )\
{\
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );\
  XLAL_CHECK ( in1 != NULL && in2 != NULL && out != NULL, XLAL_EINVAL );\
  XLAL_CHECK ( in1->length >= length && out->length >= length && out->length >= length, XLAL_EINVAL );\
  cl_int err;\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_##OP####TYPE, 0, sizeof(out->data), (void*) &(out->data)       ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_##OP####TYPE, 1, sizeof(in1->data), (const void*) &(in1->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_##OP####TYPE, 2, sizeof(in2->data), (const void*) &(in2->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
  XLAL_CHECK ( ( err = clSetKernelArg ( openclObj.kernel.kernel_##OP####TYPE, 3, sizeof(length),    (const void*) &(length)    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );\
\
  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &openclObj.kernel.kernel_##OP####TYPE, length, last ) == XLAL_SUCCESS, XLAL_EFUNC );\
\
  return XLAL_SUCCESS;\
} /* XLAL\<OP\>CLMEMVector\<TYPE\>() */ \

DEFINE_CLMEMVECTOR_MEMSET_API(REAL4);
DEFINE_CLMEMVECTOR_MEMSET_API(REAL8);
DEFINE_CLMEMVECTOR_MEMSET_API(COMPLEX8);
DEFINE_CLMEMVECTOR_V2VV_API(REAL4,Add);
DEFINE_CLMEMVECTOR_V2VV_API(REAL4,Multiply);
DEFINE_CLMEMVECTOR_V2VV_API(REAL8,Add);
DEFINE_CLMEMVECTOR_V2VV_API(REAL8,Multiply);
DEFINE_CLMEMVECTOR_V2VV_API(COMPLEX8,Add);
DEFINE_CLMEMVECTOR_V2VV_API(COMPLEX8,Multiply);

/**
 * This function create the OpenCL_OBJ if it is not initialized and will return the pointer to this object. Also it will generate some
 * basic kernels for VectorMath functions
 * */
OpenCL_OBJ *
XLALCreateOpenCL_OBJ ( void )
{
  if ( !openclObj.isInitialized )
    {
      cl_int err;
      //Get available devices and select one
      CLDEVICE *mydevice = NULL;
      XLAL_CHECK_NULL ( ( mydevice = XLALSelectCLDEVICE ( ) ) != NULL, XLAL_EFUNC );
      cl_device_id device = mydevice->id;
      openclObj.vendorID = mydevice->vendorID;
      XLALDestroyCLDEVICE ( mydevice );

      // Create a context  and associate it with one device, because clFFT seems to only work with one
      // see: https://github.com/clMathLibraries/clFFT/blob/ce107c4d5432d70321af8980a5e7fb64c4b4cce4/src/library/enqueue.cpp#L695
      openclObj.context = clCreateContext( NULL, 1, &device, NULL, NULL, &err);
      XLAL_CHECK_NULL ( err == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s", "OpenCL Create Context",  XLALGetOpenCLErrorString ( err ) );

      // Create a command queue and associate it with the selected device
      openclObj.queue = clCreateCommandQueue( openclObj.context, device, 0, &err);
      XLAL_CHECK_NULL ( err == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s", "OpenCL CreateCommandQueue",  XLALGetOpenCLErrorString ( err ) );

      // Perform runtime source compilation, and obtain kernel entry point.
      openclObj.program = 0;
      char * genericKernelSource = NULL;
      XLAL_CHECK_NULL ( ( genericKernelSource = XLALGenerateGeneralKernels () ) != NULL, XLAL_ENOMEM );
      XLAL_CHECK_NULL ( XLALGetOpenCLProgramFromSource ( genericKernelSource, &openclObj.program ) == XLAL_SUCCESS, XLAL_EFUNC );
      XLALFree ( genericKernelSource );

      /*
      * Extract the kernels entrypoint from the executable.
      */
      openclObj.kernel.kernel_MemsetREAL8      = clCreateKernel ( openclObj.program, "MemsetREAL8",      &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MemsetREAL8",      XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_MemsetCOMPLEX8   = clCreateKernel ( openclObj.program, "MemsetCOMPLEX8",   &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MemsetCOMPLEX8",   XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_MemsetREAL4      = clCreateKernel ( openclObj.program, "MemsetREAL4",      &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MemsetREAL4",      XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_AddREAL4         = clCreateKernel ( openclObj.program, "AddREAL4",         &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the AddREAL4",         XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_MultiplyREAL4    = clCreateKernel ( openclObj.program, "MultiplyREAL4",    &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MultiplyREAL4",    XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_AddREAL8         = clCreateKernel ( openclObj.program, "AddREAL8",         &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the AddREAL8",         XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_MultiplyREAL8    = clCreateKernel ( openclObj.program, "MultiplyREAL8",    &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MultiplyREAL8",    XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_AddCOMPLEX8      = clCreateKernel ( openclObj.program, "AddCOMPLEX8",      &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the AddCOMPLEX8",      XLALGetOpenCLErrorString ( err ) );
      openclObj.kernel.kernel_MultiplyCOMPLEX8 = clCreateKernel ( openclObj.program, "MultiplyCOMPLEX8", &err );
      XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", "Extract the kernel for the MultiplyCOMPLEX8", XLALGetOpenCLErrorString ( err ) );

      XLAL_CHECK_NULL ( ( err = clGetKernelWorkGroupInfo ( openclObj.kernel.kernel_MemsetREAL8, device,  CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(size_t), &openclObj.preferredWorkGroupSizeMultiple, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "Getting  CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE failed: %s", XLALGetOpenCLErrorString ( err ) );

      openclObj.refcount = 1;
      openclObj.isInitialized = ( 1 == 1 );
    }
  else
    {
      ++openclObj.refcount;
    }

  return &openclObj;
} // CreateOpenCL_OBJ()

/**
 * Releases a reference to the 'OpenCL_OBJ' , if there are no more outstanding references release and free all
 * objects and memory associate with a \c OpenCL_OBJ structure
 */
void
XLALDestroyOpenCL_OBJ ( void )
{
  if ( !openclObj.isInitialized ) {
      return;
  }

  if ( --openclObj.refcount == 0 )
    {
      XLALPrintInfo( "%s: OpenCL Object reference count = %i, freeing OpenCL Object\n", __func__, openclObj.refcount );

      XLAL_CHECK_VOID ( openclObj.refcount < 1, XLAL_EFUNC, "There are still Fstatinputs that are need this!" );
      cl_int err;
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MemsetREAL4      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MemsetREAL8      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MemsetCOMPLEX8   ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_AddREAL4         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MultiplyREAL4    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_AddREAL8         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MultiplyREAL8    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_AddCOMPLEX8      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( openclObj.kernel.kernel_MultiplyCOMPLEX8 ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s", XLALGetOpenCLErrorString ( err ) );

      XLAL_CHECK_VOID ( ( err = clReleaseProgram ( openclObj.program    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Program: %s",      XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseCommandQueue ( openclObj.queue ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release CommandQueue: %s", XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseContext( openclObj.context     ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Context: %s",      XLALGetOpenCLErrorString ( err ) );
      openclObj.isInitialized = ( 1 == 0 );
    }
  else
    {
      XLALPrintInfo( "%s: OpenCL Object reference count = %i\n", __func__, openclObj.refcount );
    }

  return;
} // XLALDestroyOpenCL_OBJ()

/**
 * Creates a CLMEMVector struct with \c length elements. The OpenCL buffer Object will have a size of
 * \f$\text{itemSize} \times \text{length}\f$, where \c length is round up to the next multiple of \c preferredWorkGroupSizeMultiple
 * of the device associated with the OpenCL_OBJ
 */
CLMEMVector *
XLALCreateCLMEMVector ( UINT4 length, /**< number of elements */
                        size_t itemSize, /**<size of one element in bytes */
                        cl_mem_flags flags /**< A bit-field that is used to specify allocation and usage information. See https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateBuffer.html for available flags */
                      )
{
  XLAL_CHECK_NULL ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  CLMEMVector *ret = NULL;
  XLAL_CHECK_NULL ( (ret = XLALCalloc ( 1, sizeof(CLMEMVector) )) != NULL, XLAL_ENOMEM );
  UINT4 size = itemSize * bestWorkSize ( length );

  cl_int err = 0;
  ret->data = clCreateBuffer ( openclObj.context, flags, size, NULL, &err );
  XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s", " OpenCL memory allocation",  XLALGetOpenCLErrorString ( err ) );
  ret->itemSize = itemSize;
  ret->length = length;

  return ret;
} // XLALCreateCLMEMVector()

/**
 * Creates a CLMEMSequence struct with \c length elements. This will internally call XLALCreateCLMEMVector()
 */
CLMEMSequence *
XLALCreateCLMEMSequence ( UINT4 length, /**< number of elements */
                          size_t itemSize, /**<size of one element in bytes */
                          cl_mem_flags flags /**< A bit-field that is used to specify allocation and usage information. See https://www.khronos.org/registry/OpenCL/sdk/1.2/docs/man/xhtml/clCreateBuffer.html for available flags */
                        )
{
  XLAL_CHECK_NULL ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  CLMEMSequence *ret = NULL;
	XLAL_CHECK_NULL ( ( ret = (CLMEMSequence*) XLALCreateCLMEMVector (length, itemSize, flags) ) != NULL, XLAL_ENOMEM );

  return ret;
} // XLALCreateCLMEMSequence()

/**
 * Free a CLMEMVector
 */
void
XLALDestroyCLMEMVector ( CLMEMVector* vec )
{
  if ( !vec ) {
    return;
  }

  cl_int err;
  XLAL_CHECK_VOID ( ( err = clReleaseMemObject ( vec->data ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release MemObject: %s", XLALGetOpenCLErrorString ( err ) );
  XLALFree ( vec );

  return;
} // XLALDestroyCLMEMVector()

/**
 * Free a MultiCLMEMVector
 */
void
XLALDestroyMultiCLMEMVector ( MultiCLMEMVector *multi )
{
  if ( !multi )
    return;

  UINT4 numDetectors = multi->length;
  for ( UINT4 X = 0; X < numDetectors; X ++ ) {
    XLALDestroyCLMEMVector ( multi->data[X] );
  }

  XLALFree ( multi->data );
  XLALFree ( multi );

  return;

} // XLALDestroyMultiCLMEMVector()

/**
 * Free a CLMEMSequence
 */
void XLALDestroyCLMEMSequence ( CLMEMSequence *mem )
{
  if ( !mem ) {
    return;
  }

  CLMEMVector *vec = (CLMEMVector*) mem;
  XLALDestroyCLMEMVector ( vec );

  return;
} // XLALDestroyCLMEMSequence()

/**
 * OpenCL version for freeing a COMPLEX8TimeSeries in which the COMPLEX8Sequence is replaced by a CLMEMSequence.
 * This is needed when transferring an MultiCOMPLEX8TimeSeries via XLALTransferMultiCOMPLEX8TimeSeriesToCLMEM to
 * an OpenCL device
 */
void XLALDestroyCOMPLEX8TimeSeries_OpenCL ( COMPLEX8TimeSeries *vec )
{
  if ( !vec ) {
    return;
  }
	XLALDestroyCLMEMSequence ( (CLMEMSequence*)vec->data);
	XLALFree( vec );

  return;
} // XLALDestroyCLMEMTimeSeries()

/**
 * Copy the first \c length elements of CLMEMVector \c in to a CLMEMVector \c out. The item sizes have to be equal
 */
int
XLALCopyCLMEMVector ( CLMEMVector *out,      /**< [out] pointer to a CLMEMVector struct */
                      const CLMEMVector *in, /**< [in] pointer to a CLMEMVector struct */
                      UINT4 length,          /**< number of elements to copy */
                      BOOLEAN last           /**< if true, this function will call XLALOpenCLFinish */
                     )
{
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( out != NULL && in != NULL, XLAL_EINVAL );
  XLAL_CHECK ( out->length >= length && in->length >= length, XLAL_EINVAL );
  XLAL_CHECK ( out->itemSize == in->itemSize, XLAL_EINVAL );

  cl_int err = clEnqueueCopyBuffer ( openclObj.queue, in->data, out->data, 0, 0, in->length*in->itemSize, 0, NULL, NULL );
  XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Request Copy CLMEMVector",  XLALGetOpenCLErrorString ( err ) );
  if ( last )
    {
      XLAL_CHECK ( XLALOpenCLFinish ( "Copy CLMEMVector failed" ) == XLAL_SUCCESS, XLAL_EFUNC );
    }

  return XLAL_SUCCESS;
} // XLALCopyCLMEMVector()

/**
 * Transfers a vector \c in to a CLMEMVector \c out on an OpenCL device, which have to have the same item size as the \c in vector.
 * The first \c length elements are transferred.
 */
int
XLALTransferVectorToCLMEMVector ( CLMEMVector *out, /**< [out] pointer to an CLMEMVector struct */
                                  const void *in,   /**< [in] pointer to an array */
                                  UINT4 length,     /**< number of elements to transfer */
                                  size_t itemSize,  /**< item size of \c in vector */
                                  BOOLEAN last      /**< if true, this function will call XLALOpenCLFinish */
                               )
{
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( in != NULL && out != NULL , XLAL_EINVAL );
  XLAL_CHECK ( out->itemSize == itemSize, XLAL_EINVAL, "ItemSizes are not equal! (%zu != %zu)", out->itemSize, itemSize );
  XLAL_CHECK ( out->length >= length, XLAL_EINVAL, "Length of output vector is too short! (%d < %d)", out->length, length );


  cl_int err = clEnqueueWriteBuffer ( openclObj.queue, out->data, CL_FALSE, 0, length * itemSize, in, 0 , NULL, NULL );
  XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Transferring host memory to GPU",  XLALGetOpenCLErrorString ( err ) );
  if ( last )
    {
      XLAL_CHECK ( XLALOpenCLFinish ( "Flush memory transfers to GPU" ) == XLAL_SUCCESS, XLAL_EFUNC );
    }

  return XLAL_SUCCESS;
} // XLALTransferVectorToCLMEMVector()

/**
 * Transfers a CLMEMVector \c in to a vector \c out on the host, which have to have the same item size as the \c in vector.
 * The first \c length elements are transferred.
 */
int
XLALGetVectorFromCLMEMVector ( void *out,             /**< [out] pointer to an array */
                               const CLMEMVector *in, /**< [in] pointer to an CLMEMVector struct */
                               UINT4 length,          /**< number of elements to transfer */
                               size_t itemSize,       /**< item size of allocated \c out vector */
                               BOOLEAN last           /**< if true, this function will call XLALOpenCLFinish */
                            )
{
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( in != NULL && out != NULL , XLAL_EINVAL );
  XLAL_CHECK ( in->itemSize == itemSize, XLAL_EINVAL, "ItemSizes are not equal! (%zu != %zu)", in->itemSize, itemSize );
  XLAL_CHECK ( length >= in->length, XLAL_EINVAL, "Length of output vector is too short! (%d < %d)", length, in->length );

  cl_int err = clEnqueueReadBuffer ( openclObj.queue, in->data, CL_FALSE, 0, in->length * itemSize, out, 0, NULL, NULL );
  XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Request transfer from CLMEMVector to host memory",  XLALGetOpenCLErrorString ( err ) );
  if ( last )
    {
      XLAL_CHECK ( XLALOpenCLFinish ( "Flush memory transfers to host memory" ) == XLAL_SUCCESS, XLAL_EFUNC );
    }

  return XLAL_SUCCESS;
} // XLALGetVectorFromCLMEMVector()

/**
 * checks if an OpenCL-capable device is available or if the environment variable CLDEVICE is
 * defined, if a device with this name is available
 */
int
XLALOpenCLIsAvaible ( void )
{
  CLDEVICE *device = XLALSelectCLDEVICE ( );
  if ( device == NULL )
    {
      return (1 == 0);
    }
  XLALDestroyCLDEVICE ( device );
  return (1 == 1);
} // XLALOpenCLIsAvaible()

/**
 * Wrapper for executing an 1D Kernel \c kernel.
 */
int
XLALExecuteKernel_OpenCL ( cl_kernel *kernel,  /**< kernel to execute */
                           UINT4 size,         /**< number of global work-items */
                           BOOLEAN last      /**< if true, this function will call XLALOpenCLFinish */
                         )
{
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( kernel != NULL, XLAL_EINVAL );
  cl_int err;
  const size_t workSize[] = {bestWorkSize(size)};
  XLAL_CHECK ( ( err = clEnqueueNDRangeKernel ( openclObj.queue, (*kernel), 1, NULL, workSize, NULL, 0, NULL, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s", __func__, XLALGetOpenCLErrorString ( err ) );;
  if ( last )
    {
      XLAL_CHECK ( XLALOpenCLFinish ( "clEnqueueNDRangeKernel" ) == XLAL_SUCCESS, XLAL_EFUNC );
    }

  return XLAL_SUCCESS;
} // XLALExecuteKernel_OpenCL()

/**
 * Wrapper for calling clFinish(), that wait until all previously queued OpenCL commands are commpleted. \c msg will be used as output when this fails.
 */
int
XLALOpenCLFinish ( const char* msg )
{
  XLAL_CHECK ( openclObj.isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  cl_int err;
  XLAL_CHECK ( ( err = clFinish ( openclObj.queue ) ) == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s",  msg,  XLALGetOpenCLErrorString ( err ) );
  return XLAL_SUCCESS;
} // XLALOpenCLFinish()

/**
 * Compiles a source code \c source for an OpenCL device and return the cl_program \c program, from which the kernels can be
 * extracted.
 */
int
XLALGetOpenCLProgramFromSource ( const char *source,  /**< [in] source code to create program for */
                                 cl_program *program  /**< [out] the resulting cl_program */
                               )
{
  XLAL_CHECK ( *program == 0, XLAL_EINVAL);
  XLAL_CHECK ( source != NULL, XLAL_EINVAL);
  const char * asource[2];
  asource [0] = headerOpenCL;
  asource [1] = source;
  cl_int err;
  cl_device_id device;
  XLAL_CHECK ( ( err = clGetContextInfo ( openclObj.context, CL_CONTEXT_DEVICES, sizeof(device), &device, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "Fail to get device from context%s", XLALGetOpenCLErrorString ( err ) );
  *program = clCreateProgramWithSource ( openclObj.context,  2,  asource, NULL, NULL );
  err = clBuildProgram ( *program, 1, &device, NULL, NULL, NULL );
  if ( err != CL_SUCCESS )
    {
      size_t logSize;
      char *buildlog = NULL;
      clGetProgramBuildInfo ( *program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize );
      XLAL_CHECK ( ( buildlog = XLALCalloc ( logSize, sizeof(char) ) ) != NULL, XLAL_ENOMEM );
      clGetProgramBuildInfo ( *program, device, CL_PROGRAM_BUILD_LOG, logSize*sizeof(char), buildlog, NULL );
      XLAL_CHECK ( err == CL_SUCCESS, XLAL_EFUNC, "clBuildProgram failed: %s . Error message: \n%s", XLALGetOpenCLErrorString ( err ) , buildlog );
    }
  return XLAL_SUCCESS;
} // XLALGetOpenCLProgramFromSource()

/**
 * Fill and return a CLPLATFORMVector with available OpenCL platforms and devices
 */
static CLPLATFORMVector*
XLALGetOpenCLPLATFORMVector ( void )
{

  cl_int err = 0;

  CLPLATFORMVector *ret=NULL;
  cl_uint numPlatforms = 0;
  cl_platform_id *platforms = NULL;

  // Get number of platforms
  XLAL_CHECK_NULL ( ( err = clGetPlatformIDs(0, NULL, &numPlatforms) ) == CL_SUCCESS, XLAL_EFUNC,"%s failed: %s", "OpenCL get number of platforms",  XLALGetOpenCLErrorString ( err ) );

  // Allocate enough space for each platform
  XLAL_CHECK_NULL ( ( platforms =  XLALCalloc ( numPlatforms, sizeof(cl_platform_id ) ) ) != NULL, XLAL_ENOMEM );

  // Fill in platforms with clGetPlatformIDs()
  XLAL_CHECK_NULL ( ( err = clGetPlatformIDs ( numPlatforms, platforms, NULL ) ) == CL_SUCCESS, XLAL_EFUNC,"%s failed: %s", "OpenCL get platforms",  XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK_NULL ( ( ret = XLALCalloc ( 1, sizeof(*ret) ) ) != NULL, XLAL_ENOMEM );
  XLAL_CHECK_NULL ( ( ret->data = XLALCalloc ( numPlatforms, sizeof(*(ret->data)) ) ) != NULL, XLAL_ENOMEM );
  ret->length = numPlatforms;
  char *completeVersion;
  char version[4];
  version[3] = 0;

  for ( UINT4 i = 0; i < numPlatforms; i++ )
    {
      XLAL_CHECK_NULL ( ( ret->data[i] =  XLALCalloc ( 1, sizeof(*(ret->data[i])) ) ) != NULL, XLAL_ENOMEM );
      cl_uint numDevices = 0;
      cl_device_id *devices = NULL;

      ret->data[i]->platform = platforms[i];
      size_t valueSize = 0;
      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_NAME, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
      XLAL_CHECK_NULL ( ( ret->data[i]->platformName = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_NAME, valueSize, ret->data[i]->platformName, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );

      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_VENDOR, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
      XLAL_CHECK_NULL ( ( ret->data[i]->platformVendor = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_VENDOR, valueSize, ret->data[i]->platformVendor, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );

      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_VERSION, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
      XLAL_CHECK_NULL ( ( completeVersion = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
      XLAL_CHECK_NULL ( ( err = clGetPlatformInfo ( platforms[i], CL_PLATFORM_VERSION, valueSize, completeVersion, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
      memcpy(version, &completeVersion[7], 3);
      ret->data[i]->platformVersion = atof(version);
      XLALFree ( completeVersion );

      XLAL_CHECK_NULL ( ( ret->data[i]->devices =  XLALCalloc ( 1, sizeof(CLDEVICEVector)) ) != NULL, XLAL_ENOMEM );

      // Get number of Devices, if there is no Devices continue
      if ( ( err = clGetDeviceIDs( platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices) ) == CL_DEVICE_NOT_FOUND )
        {
          ret->data[i]->devices->length = numDevices;
          continue;
        }

      XLAL_CHECK_NULL ( err == CL_SUCCESS, XLAL_EFUNC,"%s failed: %s",  "OpenCL get number of devices",  XLALGetOpenCLErrorString ( err ) );

      // Allocate enough space for each device
      XLAL_CHECK_NULL ( ( devices =  XLALCalloc ( numDevices, sizeof(cl_device_id ) ) ) != NULL, XLAL_ENOMEM );

      // Fill in devices with clGetDeviceIDs()
      XLAL_CHECK_NULL ( ( err = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL) ) == CL_SUCCESS, XLAL_EFUNC,"%s failed: %s",  "OpenCL get Devices",  XLALGetOpenCLErrorString ( err ) );

      XLAL_CHECK_NULL ( ( ret->data[i]->devices->data = XLALCalloc ( numDevices, sizeof(*(ret->data[i]->devices->data)) ) ) != NULL, XLAL_ENOMEM );
      ret->data[i]->devices->length = numDevices;

      for ( UINT4 j = 0; j < numDevices; j++ )
        {
          XLAL_CHECK_NULL ( ( ret->data[i]->devices->data[j] =  XLALCalloc ( 1, sizeof(CLDEVICE)) ) != NULL, XLAL_ENOMEM );
          ret->data[i]->devices->data[j]->id = devices[j];

          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_NAME, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( ret->data[i]->devices->data[j]->deviceName = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_NAME, valueSize, ret->data[i]->devices->data[j]->deviceName, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );

          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_VENDOR, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( ret->data[i]->devices->data[j]->deviceVendor = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_VENDOR, valueSize, ret->data[i]->devices->data[j]->deviceVendor, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );

          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_VERSION, 0, NULL, &valueSize ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( completeVersion = XLALCalloc ( valueSize, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_VERSION, valueSize, completeVersion, NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          memcpy(version, &completeVersion[7], 3);
          ret->data[i]->devices->data[j]->deviceVersion = atof(version);
          XLALFree ( completeVersion );

          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_TYPE, sizeof(cl_device_type), &(ret->data[i]->devices->data[j]->type), NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &(ret->data[i]->devices->data[j]->globalMem), NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &(ret->data[i]->devices->data[j]->doubleAvailable), NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
          XLAL_CHECK_NULL ( ( err = clGetDeviceInfo ( devices[j], CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &(ret->data[i]->devices->data[j]->vendorID), NULL ) ) == CL_SUCCESS, XLAL_EFUNC, "%s", XLALGetOpenCLErrorString( err ) );
        } // for j < numDevices

      XLALFree ( devices );
    } // for i < numPlatforms

  XLALFree ( platforms );

  return ret;
} // XLALGetOpenCLPLATFORMVector()

static void
XLALDestroyCLPLATFORMVector ( CLPLATFORMVector *vec )
{
  if ( ! vec ) {
    return;
  }
  if ( vec->data != NULL )
    {
      UINT4 length = vec->length;
      for ( UINT4 i=0; i < length; i ++ ) {
        XLALDestroyCLPLATFORM ( vec->data[i] );
      } // for i < length
      XLALFree ( vec->data );
    } // if vec->data

  XLALFree ( vec );

  return;
} // XLALDestroyCLPLATFORMVector()

static void
XLALDestroyCLPLATFORM ( CLPLATFORM *platform )
{
  if ( ! platform ) {
    return;
  }
  if ( platform->devices != NULL )
    {
      XLALDestroyCLDEVICEVector ( platform->devices );
    }
  if ( platform->platformName != NULL )
    {
      XLALFree ( platform->platformName );
    }
  if ( platform->platformVendor != NULL )
    {
      XLALFree ( platform->platformVendor );
    }

  XLALFree ( platform );

  return;
} // XLALDestroyCLPLATFORM()

static void
XLALDestroyCLDEVICEVector ( CLDEVICEVector *vec )
{
  if ( ! vec ) {
    return;
  }
  if ( vec->data != NULL )
    {
      UINT4 length = vec->length;
      for ( UINT4 i=0; i < length; i ++ ) {
        XLALDestroyCLDEVICE ( vec->data[i] );
      } // for i < length
      XLALFree ( vec->data );
    } // if vec->data

  XLALFree ( vec );

  return;
} // XLALDestroyCLDEVICEVector()

static void
XLALDestroyCLDEVICE ( CLDEVICE *device )
{
  if ( ! device ) {
      return;
  }
  if ( device->deviceName != NULL )
    {
      XLALFree ( device->deviceName );
    }
  if ( device->deviceVendor != NULL )
    {
      XLALFree ( device->deviceVendor );
    }

  XLALFree ( device );

  return;
} // XLALDestroyCLDEVICE()

/**
 * Function for selecting an OpenCL-capable device. If the environment CLDEVICE is defined it will only search
 * for this device, otherwise it will select the GPU with biggest global memory and when there is no GPU device
 * any other device. The device and platform must support at least OpenCL 1.2 and double floating-point precision
 */
static CLDEVICE*
XLALSelectCLDEVICE ( void )
{
  CLDEVICE *select = NULL;
  CLDEVICE *ret = NULL;
  CLPLATFORMVector *platforms = NULL;
  const char *deviceName = getenv("CLDEVICE");

  XLAL_CHECK_NULL ( ( platforms = XLALGetOpenCLPLATFORMVector() ) != NULL, XLAL_EFUNC );
  // find GPU device with biggest global Memory or device defined by environment variable CLDEVICE
  for ( UINT4 i = 0;  i< platforms->length; i++ )
    {
      if ( platforms->data[i]->platformVersion < 1.2 ) {
          continue;
      }
      for ( UINT4 j = 0; j < platforms->data[i]->devices->length; j++ )
        {
          if ( deviceName == NULL )
            {
              if ( platforms->data[i]->devices->data[j]->deviceVersion < 1.2 ) {
                  continue;
              }
              if ( platforms->data[i]->devices->data[j]->type == CL_DEVICE_TYPE_GPU && platforms->data[i]->devices->data[j]->doubleAvailable != 0 )
                {
                  if ( select == NULL )
                    {
                      select = platforms->data[i]->devices->data[j];
                    }
                  else if ( platforms->data[i]->devices->data[j]->globalMem > select->globalMem )
                    {
                      select = platforms->data[i]->devices->data[j];
                    }
                }
            }
          else if ( strcmp ( deviceName, platforms->data[i]->devices->data[j]->deviceName ) == 0 )
            {
              select = platforms->data[i]->devices->data[j];
            }
        }
    } // find selected device or 'best' GPU device

  if ( deviceName != NULL && select == NULL )
      {
        XLALPrintError ( "%s: Device %s not found\n",__func__, deviceName);
      }
  // if no GPU device is found and no device is given choose another device
  else if ( select == NULL )
    {
      for ( UINT4 i = 0;  i< platforms->length; i++ )
        {
          if ( platforms->data[i]->platformVersion < 1.2 ) {
              continue;
          }
          for ( UINT4 j = 0; j < platforms->data[i]->devices->length; j++ )
            {
              if ( platforms->data[i]->devices->data[j]->deviceVersion < 1.2 ) {
                  continue;
              }
              if ( platforms->data[i]->devices->data[j]->doubleAvailable != 0 )
                {
                  if ( select == NULL )
                    {
                      select = platforms->data[i]->devices->data[j];
                    }
                  else if ( platforms->data[i]->devices->data[j]->globalMem > select->globalMem )
                    {
                      select = platforms->data[i]->devices->data[j];
                    }
                }
            }
        }
    }

  // if we found a device copy it into the return struct
  if ( select != NULL )
    {
      XLAL_CHECK_NULL ( ( ret =  XLALCalloc ( 1, sizeof(CLDEVICE)) ) != NULL, XLAL_ENOMEM );
      ret->id = select->id;
      ret->type = select->type;
      ret->globalMem = select->globalMem;
      ret->deviceVersion = select->deviceVersion;
      ret->vendorID = select->vendorID;
      size_t len = strlen(select->deviceName)+1;
      XLAL_CHECK_NULL ( ( ret->deviceName = XLALCalloc ( len, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
      memcpy(ret->deviceName, select->deviceName, len);
      len = strlen(select->deviceVendor)+1;
      XLAL_CHECK_NULL ( ( ret->deviceVendor = XLALCalloc ( len, sizeof(char) ) ) !=NULL, XLAL_ENOMEM );
      memcpy(ret->deviceVendor, select->deviceVendor, len);
      XLALPrintInfo("INFO: The device %s was selected\n", ret->deviceName);
    }

  XLALDestroyCLPLATFORMVector ( platforms );

  return ret;
} // XLALSelectCLDEVICE()

/**
 * This function generates the source code string for the OpenCL VectorMath functions and return it
 */
static char*
XLALGenerateGeneralKernels ( void )
{
  char * ret = NULL;
  XLAL_CHECK_NULL ( ( ret = XLALCalloc (1780,sizeof(char)) ) != NULL, XLAL_ENOMEM ); //1779 is current string length
  const char * types[] = { "REAL4", "REAL8", "COMPLEX8" };
  const char * op[] = { "Add", "Multiply", "Memset"};
  const char * parts[] = { "", "{\n", "UINT4 j = get_global_id(0);\n", "if ( j >= Length ) {\n", "return;\n", "}\n", "", "}\n" };
  // loop over number of operations
  for ( UINT4 j = 0; j < 3 ; j++ )
    {
      // loop over number of types
      for ( UINT4 i = 0;  i < 3; i++ )
        {
          // loop over number of lines
          for ( UINT4 k = 0; k < 8; k++ )
            {
              char temp[150];
              switch (k)
                {
                case 0:
                  if ( j < 2 ) {
                    sprintf(temp, " __kernel void %s%s ( __global %s *out, __global const %s *in1, __global const %s *in2, UINT4 Length )\n", op[j], types[i], types[i], types[i], types [i]);
                  }
                  else {
                    sprintf(temp, " __kernel void %s%s ( __global %s *in, const %s scalar, UINT4 Length )\n", op[j], types[i], types[i], types[i] );
                  }
                  break;
                case 6:
                  switch (j)
                    {
                    case 0:
                      sprintf(temp, "out[j] = in1[j] + in2[j];\n");
                      break;
                    case 1:
                      if ( i != 2 ) {
                          sprintf(temp, "out[j] = in1[j] * in2[j];\n");
                      }
                      else {
                          sprintf(temp, "out[j] = local_cmulf(in1[j], in2[j]);\n");
                      }
                      break;
                    case 2:
                          sprintf(temp, "in[j] = scalar;\n");
                          break;
                  }
                  break;
              default:
                  sprintf(temp, parts [k]);
                }
              strcat(ret,temp);
            } //for number of lines
        } // for number of types
    } // for number of operations

  return ret;
} // XLALGenerateGeneralKernels ()

/**
 * returns a human readable error string
 * taken from https://stackoverflow.com/questions/24326432/convenient-way-to-show-opencl-error-codes
 */
const char *XLALGetOpenCLErrorString ( cl_int error )
{
  switch(error)
    {
      // run-time and JIT compiler errors
      case 0: return "CL_SUCCESS";
      case -1: return "CL_DEVICE_NOT_FOUND";
      case -2: return "CL_DEVICE_NOT_AVAILABLE";
      case -3: return "CL_COMPILER_NOT_AVAILABLE";
      case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
      case -5: return "CL_OUT_OF_RESOURCES";
      case -6: return "CL_OUT_OF_HOST_MEMORY";
      case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
      case -8: return "CL_MEM_COPY_OVERLAP";
      case -9: return "CL_IMAGE_FORMAT_MISMATCH";
      case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
      case -11: return "CL_BUILD_PROGRAM_FAILURE";
      case -12: return "CL_MAP_FAILURE";
      case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
      case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
      case -15: return "CL_COMPILE_PROGRAM_FAILURE";
      case -16: return "CL_LINKER_NOT_AVAILABLE";
      case -17: return "CL_LINK_PROGRAM_FAILURE";
      case -18: return "CL_DEVICE_PARTITION_FAILED";
      case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

      // compile-time errors
      case -30: return "CL_INVALID_VALUE";
      case -31: return "CL_INVALID_DEVICE_TYPE";
      case -32: return "CL_INVALID_PLATFORM";
      case -33: return "CL_INVALID_DEVICE";
      case -34: return "CL_INVALID_CONTEXT";
      case -35: return "CL_INVALID_QUEUE_PROPERTIES";
      case -36: return "CL_INVALID_COMMAND_QUEUE";
      case -37: return "CL_INVALID_HOST_PTR";
      case -38: return "CL_INVALID_MEM_OBJECT";
      case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
      case -40: return "CL_INVALID_IMAGE_SIZE";
      case -41: return "CL_INVALID_SAMPLER";
      case -42: return "CL_INVALID_BINARY";
      case -43: return "CL_INVALID_BUILD_OPTIONS";
      case -44: return "CL_INVALID_PROGRAM";
      case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
      case -46: return "CL_INVALID_KERNEL_NAME";
      case -47: return "CL_INVALID_KERNEL_DEFINITION";
      case -48: return "CL_INVALID_KERNEL";
      case -49: return "CL_INVALID_ARG_INDEX";
      case -50: return "CL_INVALID_ARG_VALUE";
      case -51: return "CL_INVALID_ARG_SIZE";
      case -52: return "CL_INVALID_KERNEL_ARGS";
      case -53: return "CL_INVALID_WORK_DIMENSION";
      case -54: return "CL_INVALID_WORK_GROUP_SIZE";
      case -55: return "CL_INVALID_WORK_ITEM_SIZE";
      case -56: return "CL_INVALID_GLOBAL_OFFSET";
      case -57: return "CL_INVALID_EVENT_WAIT_LIST";
      case -58: return "CL_INVALID_EVENT";
      case -59: return "CL_INVALID_OPERATION";
      case -60: return "CL_INVALID_GL_OBJECT";
      case -61: return "CL_INVALID_BUFFER_SIZE";
      case -62: return "CL_INVALID_MIP_LEVEL";
      case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
      case -64: return "CL_INVALID_PROPERTY";
      case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
      case -66: return "CL_INVALID_COMPILER_OPTIONS";
      case -67: return "CL_INVALID_LINKER_OPTIONS";
      case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

      // extension errors
      case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
      case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
      case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
      case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
      case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
      case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";

      // clfft errors
      case 4096: return "CLFFT_BUGCHECK";
      case 4097: return "CLFFT_NOTIMPLEMENTED";
      case 4098: return "CLFFT_TRANSPOSED_NOTIMPLEMENTED";
      case 4099: return "CLFFT_FILE_NOT_FOUND";
      case 4100: return "CLFFT_FILE_CREATE_FAILURE";
      case 4101: return "CLFFT_VERSION_MISMATCH";
      case 4102: return "CLFFT_INVALID_PLAN";
      case 4103: return "CLFFT_DEVICE_NO_DOUBLE";
      case 4104: return "CLFFT_DEVICE_MISMATCH";
      case 4105: return "CLFFT_ENDSTATUS";

      default: return "Unknown OpenCL error";
    }
} // XLALGetOpenCLErrorString ()
