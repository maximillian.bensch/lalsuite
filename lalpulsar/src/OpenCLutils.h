/*
 *  Copyright (C) 2018 Maximillian Bensch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */
#ifndef _OPENCLUTILS_h  /* Double-include protection. */
#define _OPENCLUTILS_h

#include <lal/LALStdlib.h>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

/* C++ protection. */
#ifdef  __cplusplus
extern "C" {
#endif

/**
 * \defgroup OpenCLutils_h Header OpenCLutils.h
 * \authors Maximillian Bensch
 * \ingroup lalpulsar_general
 *
 * \brief Functions to execute code with OpenCL
 * \date 2018
 * \code
 * #include <lal/OpenCLutils.h>
 * \endcode
 */

// @{


/*---------- DEFINES ----------*/

// ---------- Shared global variables ---------- //

/*---------- exported types ----------*/
/** Struct of kernels of generic VectorMath functions */
typedef struct tagOpenCLGernericKernels
{
  cl_kernel kernel_MemsetREAL4;
  cl_kernel kernel_MemsetREAL8;
  cl_kernel kernel_MemsetCOMPLEX8;
  cl_kernel kernel_AddREAL4;
  cl_kernel kernel_MultiplyREAL4;
  cl_kernel kernel_AddREAL8;
  cl_kernel kernel_MultiplyREAL8;
  cl_kernel kernel_AddCOMPLEX8;
  cl_kernel kernel_MultiplyCOMPLEX8;
} OpenCLGernericKernels;

/** Struct of needed objects for executing OpenCL kernels */
typedef struct tagOpenCL_OBJ
{
 cl_program program; /**< cl_program for vectormath kernels */
 cl_context context; /**< context for the queue associated with one device */
 cl_command_queue queue; /**< OpenCL command queue */
 OpenCLGernericKernels kernel; /**< Generic vectormath kernels */
 size_t preferredWorkGroupSizeMultiple; /**< preferred WorkGroup-size multiple of this context */
 cl_uint vendorID; /**< Vendor ID of selected device for choosing vendor specific code */
 int refcount; /**< Reference counter */
 BOOLEAN isInitialized; /** flag if OpenCL object is initialized */
} OpenCL_OBJ;

typedef cl_mem CLMEM;	/**< OpenCL Mem Object */

 /** Vector of type CLMEM */
typedef struct tagCLMEMVector
{
  CLMEM data;      /**< CLMEM object for the data array. */
  size_t itemSize; /**< itemsize of one element in array. */
  UINT4 length;    /**< Number of elements in array. */
} CLMEMVector;

typedef CLMEMVector CLMEMSequence; /**< CLMEM sequence */

/** A multi-detector vector of CLMEMVector. */
typedef struct tagMultiCLMEMVector
{
  CLMEMVector **data; /** CLMEMVector for each detector */
  UINT4 length; /**< number of detectors */
} MultiCLMEMVector;

/*---------- exported prototypes [API] ----------*/
OpenCL_OBJ *XLALCreateOpenCL_OBJ ( void );
CLMEMVector * XLALCreateCLMEMVector ( UINT4 length, size_t itemSize, cl_mem_flags flags );
CLMEMSequence * XLALCreateCLMEMSequence ( UINT4 length, size_t itemSize, cl_mem_flags flags );

int XLALOpenCLIsAvaible ( void );
int XLALExecuteKernel_OpenCL ( cl_kernel *kernel, UINT4 size, BOOLEAN last );
int XLALGetOpenCLProgramFromSource ( const char * source, cl_program *program);
int XLALOpenCLFinish ( const char* msg );

int XLALCopyCLMEMVector ( CLMEMVector *out, const CLMEMVector *in, UINT4 length, BOOLEAN last );
int XLALGetVectorFromCLMEMVector ( void *out, const CLMEMVector *in, UINT4 length, size_t itemSize, BOOLEAN last );
int XLALTransferVectorToCLMEMVector ( CLMEMVector *out, const void *in, UINT4 length, size_t itemSize, BOOLEAN last );
const char *XLALGetOpenCLErrorString ( cl_int error );

void XLALDestroyOpenCL_OBJ ( void );
void XLALDestroyCLMEMVector ( CLMEMVector *vec );
void XLALDestroyCLMEMSequence ( CLMEMSequence *vec );
void XLALDestroyCOMPLEX8TimeSeries_OpenCL ( COMPLEX8TimeSeries *vec );
void XLALDestroyMultiCLMEMVector ( MultiCLMEMVector *multi );

/** Set \f$\text{in} = \text{scalar}\f$ over a REAL4 CLMEMVector \c in */
int XLALMemsetCLMEMVectorREAL4 ( CLMEMVector *in, const REAL4 scalar, BOOLEAN last );
/** Set \f$\text{in} = \text{scalar}\f$ over a REAL8 CLMEMVector \c in */
int XLALMemsetCLMEMVectorREAL8 ( CLMEMVector *in, const REAL8 scalar, BOOLEAN last );
/** Set \f$\text{in} = \text{scalar}\f$ over a COMPLEX8 CLMEMVector \c in */
int XLALMemsetCLMEMVectorCOMPLEX8 ( CLMEMVector *in, const COMPLEX8 scalar, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} + \text{in2}\f$ over REAL4 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALAddCLMEMVectorREAL4 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} \times \text{in2}\f$ over REAL4 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALMultiplyCLMEMVectorREAL4 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} + \text{in2}\f$ over REAL8 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALAddCLMEMVectorREAL8 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} \times \text{in2}\f$ over REAL8 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALMultiplyCLMEMVectorREAL8 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} + \text{in2}\f$ over COMPLEX8 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALAddCLMEMVectorCOMPLEX8 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );
/** Compute \f$\text{out} = \text{in1} \times \text{in2}\f$ over COMPLEX8 CLMEMVectors \c in1 and \c in2 with \c len elements with OpenCL*/
int XLALMultiplyCLMEMVectorCOMPLEX8 ( CLMEMVector *out, const CLMEMVector *in1, const CLMEMVector *in2, UINT4 len, BOOLEAN last );

// @}

#ifdef  __cplusplus
}
#endif
/* C++ protection. */

#endif  /* Double-include protection. */
