#define XLAL_BILLION_REAL8   1e9
#define XLAL_I_BILLION_REAL8 1e-9
#define LD_SMALL4       (2.0e-4)                // "small" number for REAL4: taken from Demod()
#define LAL_PI        3.141592653589793238462643383279502884
#define OOPI         (1.0 / LAL_PI)      // 1/pi
#define GPSGETREAL8(v) ( (v).x + ( (v).y * XLAL_I_BILLION_REAL8 ) );
#define MYMAX(x,y) ( (x) > (y) ? (x) : (y) )
#define MYMIN(x,y) ( (x) < (y) ? (x) : (y) )
#define SQ(x) ( (x) * (x) )

__constant REAL8 LAL_FACT_INV[21] = { \
  1., \
  1., \
  0.5, \
  0.16666666666666666666666666666666667, \
  0.041666666666666666666666666666666667, \
  0.0083333333333333333333333333333333333, \
  0.0013888888888888888888888888888888889, \
  0.00019841269841269841269841269841269841, \
  0.000024801587301587301587301587301587302, \
  2.7557319223985890652557319223985891e-6, \
  2.7557319223985890652557319223985891e-7, \
  2.5052108385441718775052108385441719e-8, \
  2.0876756987868098979210090321201432e-9, \
  1.6059043836821614599392377170154948e-10, \
  1.1470745597729724713851697978682106e-11, \
  7.6471637318198164759011319857880704e-13, \
  4.779477332387385297438207491117544e-14, \
  2.81145725434552076319894558301032e-15, \
  1.5619206968586226462216364350057333e-16, \
  8.2206352466243297169559812368722807e-18, \
  4.1103176233121648584779906184361404e-19 \
};

__kernel void detectorFrameSample(UINT4 subwork_size, REAL8 refTime8, REAL8 Tsft, REAL8 tStart_SRC_0, REAL8 dt_SRC, UINT4 numSamples_SRCX,
	REAL8 fHet, REAL8 tStart_DET_0,
	__global const REAL8 *SRCtimesX_Tdot,__global const REAL8 *SRCtimesX_DeltaT,__global const LIGOTimeGPS *Timestamps_DETX,__global const REAL4 *AMcoefX_a,__global const REAL4 *AMcoefX_b,
	__global REAL8 *ti_DET, __global COMPLEX8 * TStmp1_SRC, __global COMPLEX8 *TStmp2_SRC, UINT4 numSFTsX)
{
	REAL4 i_subwork_size = 1.0f / (REAL4) subwork_size;
	
	// get index of upper  & inner loops
	UINT4 alpha = (UINT4) (get_global_id(0) * i_subwork_size);
	// Exit if we overflow on alpha (because the global work size has to be multiple of work group)....
	if (alpha >= numSFTsX) return;
	UINT4 j = get_global_id(0) - (alpha * subwork_size);
	// FIXME!!!!!
	//if (j == subwork_size-1) {
	//	alpha ++;
	//	j = 0;
	//}
	
	// define some useful shorthands
	REAL8 Tdot_al  = SRCtimesX_Tdot[ alpha ];					// the instantaneous time derivitive dt_SRC/dt_DET at the MID-POINT of the SFT
	REAL8 tMid_SRC_al   = refTime8 + SRCtimesX_DeltaT[alpha];	// MID-POINT time of the SFT at the SRC
	REAL8 tStart_SRC_al = tMid_SRC_al - 0.5 * Tsft * Tdot_al;	// approximate START time of the SFT at the SRC
	REAL8 tEnd_SRC_al   = tMid_SRC_al + 0.5 * Tsft * Tdot_al;	// approximate END time of the SFT at the SRC

	REAL8 tStart_DET_al = GPSGETREAL8 ( Timestamps_DETX[alpha] );// START time of the SFT at the detector
	REAL8 tMid_DET_al   = tStart_DET_al + 0.5 * Tsft;			// MID-POINT time of the SFT at the detector

	// indices of first and last SRC-frame sample corresponding to this SFT
	UINT4 iStart_SRC_al = ceil ( (tStart_SRC_al - tStart_SRC_0) / dt_SRC );	// the index of the resampled timeseries corresponding to the start of the SFT
	UINT4 iEnd_SRC_al   = floor ( (tEnd_SRC_al - tStart_SRC_0) / dt_SRC );	// the index of the resampled timeseries corresponding to the end of the SFT

	// truncate to actual SRC-frame timeseries
	iStart_SRC_al = MYMIN ( iStart_SRC_al, numSamples_SRCX - 1);
	iEnd_SRC_al   = MYMAX ( iEnd_SRC_al, numSamples_SRCX - 1);
	UINT4 numSamplesSFT_SRC_al = iEnd_SRC_al - iStart_SRC_al + 1;		// the number of samples in the SRC-frame for this SFT

	if (j < numSamplesSFT_SRC_al) {
		REAL4 a_al = AMcoefX_a[alpha];
		REAL4 b_al = AMcoefX_b[alpha];

		UINT4 iSRC_al_j  = iStart_SRC_al + j;

		// for each time sample in the SRC frame, we estimate the corresponding detector time,
		// using a linear approximation expanding around the midpoint of each SFT
		REAL8 t_SRC = tStart_SRC_0 + iSRC_al_j * dt_SRC;
		REAL8 tmp = tMid_DET_al + ( t_SRC - tMid_SRC_al ) / Tdot_al;
		ti_DET [ iSRC_al_j ] = tmp;

		// pre-compute correction factors due to non-zero heterodyne frequency of input
		REAL8 tDiff = iSRC_al_j * dt_SRC + (tStart_DET_0 - tmp); 	// tSRC_al_j - tDET(tSRC_al_j)
		REAL4 cycles = 2*LAL_PI*fmod ( fHet * tDiff, 1.0 );			// the accumulated heterodyne cycles

		// the real and imaginary parts of the phase correction - cycles truncated to float - double cosine to expensive.
		COMPLEX8 ei2piphase = (COMPLEX8) ( cos(-cycles), sin(-cycles) );

		// apply AM coefficients a(t), b(t) to SRC frame timeseries [alternate sign to get final FFT return DC in the middle]
		REAL4 signum = (iSRC_al_j & 1)  == 0 ? 1 : -1;	// alternating sign, avoid branching
		ei2piphase *= signum;
		TStmp1_SRC[ iSRC_al_j ] = ei2piphase * a_al;
		TStmp2_SRC[ iSRC_al_j ] = ei2piphase * b_al;
	}
}

__kernel void XLALSincInterpolateCOMPLEX8TimeSeries ( __global COMPLEX8 *y_out, __global const REAL8 *t_out, __global const COMPLEX8 *ts_in, UINT4 Dterms, UINT4 numSamplesIn, REAL8 dt, const REAL8 oodt, REAL8 tmin, __global const REAL8 *win, UINT4 numSamplesOut) {

	UINT4 l = get_global_id(0);
    if ( l >= numSamplesOut ) {
       return;
    }

	REAL8 t = t_out[l] - tmin;		// measure time since start of input timeseries

	// samples outside of input timeseries are returned as 0
	if ( (t < 0) || (t > (numSamplesIn-1)*dt) )	 {	// avoid any extrapolations!
		y_out[l] = 0;
		return;
    }
    REAL8 t_by_dt = t  * oodt;
    INT8 jstar = round ( t_by_dt );		// bin closest to 't', guaranteed to be in [0, numSamples-1]

    if ( fabs ( t_by_dt - jstar ) < LD_SMALL4 )	{	// avoid numerical problems near peak
		y_out[l] = ts_in[jstar];	// known analytic solution for exact bin
		return;
	}
	
    INT4 jStart0 = jstar - Dterms;
    UINT4 jEnd0 = jstar + Dterms;
    UINT4 jStart = MYMAX ( jStart0, 0 );
    UINT4 jEnd   = MYMIN ( jEnd0, numSamplesIn - 1 );

	REAL4 delta_jStart = (t_by_dt - jStart);
	REAL4 pi_d = LAL_PI * delta_jStart;		// make sure sin/cos are float computed
	REAL4 sin0 = sin(pi_d);
	REAL4 cos0 = cos(pi_d);
	REAL4 sin0oopi = sin0 * OOPI;

	COMPLEX8 y_l = 0;
	REAL4 delta_j = delta_jStart;
	for ( UINT4 j = jStart; j <= jEnd; j ++ ) {
		COMPLEX8 Cj = win [j-jStart0] * sin0oopi / delta_j;

		y_l += Cj * ts_in[j];

		sin0oopi = -sin0oopi;		// sin-term flips sign every step
		delta_j = delta_j - 1.0 ;
	} // for j in [j* - Dterms, ... ,j* + Dterms]

	y_out[l] = y_l;
}

__kernel void XLALApplySpindownAndFreqShift(__global COMPLEX8 *xOut,__global const COMPLEX8 *xIn,__global const REAL8 *fkdot,
		REAL8 dt, REAL8 Dtau0, REAL8 freqShift,UINT4 s_max, UINT4 real_size ) {
			
	UINT4 j = get_global_id(0);
	if (j >= real_size) return;	// actual size may be bigger due to group size
	
	REAL8 taup_j = j * dt;
	REAL8 Dtau_alpha_j = Dtau0 + taup_j;
	REAL8 cycles = - freqShift * taup_j;
	REAL8 Dtau_pow_kp1 = Dtau_alpha_j;
	
	for ( UINT4 k = 1; k <= s_max; k++ ) {
		Dtau_pow_kp1 *= Dtau_alpha_j;
		cycles += - LAL_FACT_INV[k+1] * fkdot[k] * Dtau_pow_kp1;
	} // for k = 1 ... s_max

	REAL4 a = 2*M_PI*cycles;
	COMPLEX8 em2piphase = (COMPLEX8) ( cos(a), sin(a) );

	// weight the complex timeseries by the antenna patterns
	xOut[j] = local_cmulf(em2piphase , xIn[j]);
}

__kernel void resampFabX_Raw(__global COMPLEX8 *FaX_k,__global const COMPLEX8 *FabX_Raw, UINT4 offset_bins, UINT4 decimateFFT, UINT4 numFreqBins)
{
	UINT4 k = get_global_id(0);
    if ( k >= numFreqBins ) {
        return;
    }
	
    FaX_k[k] = FabX_Raw [ offset_bins + k * decimateFFT ];
}

__kernel void normalizeFaFb(__global COMPLEX8 *FaX_k,__global COMPLEX8 *FbX_k,REAL8 dtauX,REAL8 dFreq,REAL8 FreqOut0,REAL8 dt_SRC, UINT4 numFreqBins)
{
	UINT4 k = get_global_id(0);
    if ( k >= numFreqBins ) {
        return;
    }
	
	REAL8 f_k = FreqOut0 + k * dFreq;
	REAL8 cycles = - f_k * dtauX;
	// move in [0..2] to avoid precision losses when converting to float.
	cycles = cycles - rint(cycles) + 1.0;
	REAL4 a = (REAL4)cycles * 2 * M_PI;
	COMPLEX8 em2piphase = (COMPLEX8) ( cos(a), sin(a) );
	COMPLEX8 normX_k = (REAL4)dt_SRC * em2piphase;
	FaX_k[k] = local_cmulf(FaX_k[k],normX_k);
	FbX_k[k] = local_cmulf(FbX_k[k],normX_k);
}

/**
 * Fa_k can be FaX_k when we compute 2F for all detectors
 * same remark for Fb_k
 * */
__kernel void XLALComputeFstatFromFaFb(__global REAL4 *twoFPerDet, __global const COMPLEX8 *Fa_k, __global const COMPLEX8 *Fb_k,REAL4 A,REAL4 B,REAL4 C,REAL4 E,REAL4 Dinv, UINT4 numFreqBins)
{
	UINT4 k = get_global_id(0);
    if ( k >= numFreqBins ) {
        return;
    }
	
	COMPLEX8 a = Fa_k[k];
	REAL4 Fa_re = a.x;
	REAL4 Fa_im = a.y;
	COMPLEX8 b = Fb_k[k];
	REAL4 Fb_re = b.x;
	REAL4 Fb_im = b.y;

    twoFPerDet[k] = 4;	// default fallback = E[2F] in noise when Dinv == 0 due to ill-conditionness of M_munu
    if ( Dinv > 0 )
      {
        twoFPerDet[k] = 2.0f * Dinv * (  B * ( SQ(Fa_re) + SQ(Fa_im) )
                                         + A * ( SQ(Fb_re) + SQ(Fb_im) )
                                         - 2.0 * C * (   Fa_re * Fb_re + Fa_im * Fb_im )
                                         - 2.0 * E * ( - Fa_re * Fb_im + Fa_im * Fb_re )           // nonzero only in RAA case where Ed!=0
                                         );
      }
} // XLALComputeFstatFromFaFb()
