//
// Copyright (C) 2017,2018 Maximillian Bensch
// Copyright (C) 2016 Christophe Choquet
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with with program; see the file COPYING. If not, write to the
// Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA
//

#include <stdlib.h>
#include <stdio.h>

#include "ComputeFstat_Resamp.h"

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#ifdef HAVE_LIBCLFFT
#include <clFFT.h>
#endif
#ifdef HAVE_LIBECLFFT
#include <eclfft.h>
#endif
#include <lal/OpenCLutils.h>
#include "ComputeFstat_Resamp_OpenCL_kernels_cl.h"

#include <lal/TimeSeries.h>
#include <lal/Sequence.h>
#include <lal/Units.h>
#include <lal/Window.h>
#include <lal/LogPrintf.h>	 // for timing function XLALGetCPUTime()

// ========== Resamp internals ==========
static OpenCL_OBJ *openclObjLocal = NULL;
// ----- local macros ----------
#define MYMAX(x,y) ( (x) > (y) ? (x) : (y) )
#define MYMIN(x,y) ( (x) < (y) ? (x) : (y) )

// ----- local constants

// ----- local types ----------

typedef enum  tagCLFFTType {
    NONE,
    CLFFT,
   ECLFFT,
} CLFFTType;

typedef struct {
  int (*computefft_func) ( void *, CLMEMVector *, CLMEMVector *);
  void *(*initialfft_func) ( UINT4 );
  void (*fftplan_destroy_funnc) ( void * );
  void *fftplan;
} FFTfuncs;

typedef struct
{
  cl_kernel kernel_DetectorFrameSample;
  cl_kernel kernel_SincInterpolate;
  cl_kernel kernel_ApplySpindownAndFreqShift;
  cl_kernel kernel_ResampFabX_Raw;
  cl_kernel kernel_NormalizeFaFb;
  cl_kernel kernel_XLALComputeFstatFromFaFb;
  cl_program resampProgramm;
  UINT4 refcount;
} ResampOpenCLKernels;

static ResampOpenCLKernels kernel;

// ----- workspace ----------
typedef struct tagResampOpenCLWorkspace
{
  // intermediate quantities to interpolate and operate on SRC-frame timeseries
  CLMEMVector *TStmp1_SRC;	// can hold a single-detector SRC-frame spindown-corrected timeseries [without zero-padding]
  CLMEMVector *TStmp2_SRC;	// can hold a single-detector SRC-frame spindown-corrected timeseries [without zero-padding]
  CLMEMVector *SRCtimes_DET;	// holds uniformly-spaced SRC-frame timesteps translated into detector frame [for interpolation]

  // input padded timeseries ts(t) and output Fab(f) of length 'numSamplesFFT' and corresponding fftw plan
  UINT4 numSamplesFFTAlloc;	// allocated number of zero-padded SRC-frame time samples (related to dFreq)
  CLMEMVector *TS_FFT;		// zero-padded, spindown-corr SRC-frame TS

  // arrays of size numFreqBinsOut over frequency bins f_k:
  CLMEMVector *FaX_k;		// properly normalized F_a^X(f_k) over output bins
  CLMEMVector *FbX_k;		// properly normalized F_b^X(f_k) over output bins
  CLMEMVector *Fa_k;		// properly normalized F_a(f_k) over output bins
  CLMEMVector *Fb_k;		// properly normalized F_b(f_k) over output bins
  UINT4 numFreqBinsAlloc;	// internal: keep track of allocated length of frequency-arrays

  CLMEMVector *twoFPerDet; // CLMEMVector for temporarily saving FstatResults
  CLMEMVector *fkdot;  // buffer for fkdot array
  CLMEMVector *Tdot;  // buffer for SSBtimes->Tdot
  CLMEMVector *DeltaT; // buffer for SSBtimes->Tdot
  CLMEMVector *a;    // buffer for AMCoeffs->a
  CLMEMVector *b;    // buffer for AMCoeffs->b
} ResampOpenCLWorkspace;

// ----- local prototypes ----------

int XLALSetupFstatResampOpenCL ( FstatCommon *common, UINT4 numSamplesMax_SRC, ResampMethodData *resamp, FstatMethodFuncs *funcs );

static int
XLALComputeFstatResamp_OpenCL ( FstatResults* Fstats,
                                const FstatCommon *common,
                                void *method_data
                              );

static int
XLALApplySpindownAndFreqShift_OpenCL ( CLMEMVector *xOut,
                                       const COMPLEX8TimeSeries *xIn,
                                       const PulsarDopplerParams *doppler,
                                       REAL8 freqShift,
                                       CLMEMVector *fkdot
                                       );

int
XLALBarycentricResampleMultiCOMPLEX8TimeSeries_OPENCL ( ResampMethodData *resamp,
                                                        const FstatCommon *common,
                                                        MultiSSBtimes *multiSRCtimes
                                                        );

static int
XLALComputeFaFb_Resamp_OpenCL ( ResampMethodData *resamp,
                                ResampOpenCLWorkspace *ws,
                                const PulsarDopplerParams thisPoint,
                                REAL8 dFreq,
                                UINT4 numFreqBins,
                                const COMPLEX8TimeSeries *TimeSeries_SRC_a,
                                const COMPLEX8TimeSeries *TimeSeries_SRC_b
                                );
static int
XLALComputeFstatFromFaFb_OpenCL ( CLMEMVector *twoFPerDet, const CLMEMVector *Fa_k, const CLMEMVector *Fb_k, REAL4 A, REAL4 B, REAL4 C, REAL4 E, REAL4 Dinv, UINT4 size);

static int
XLALResampFabX_Raw_OpenCL ( CLMEMVector * FxX_k, const CLMEMVector * TS_FFT, UINT4 offset_bins, UINT4 decimateFFT, UINT4 size );

static int
XLALNormalizeFaFb_OpenCL ( CLMEMVector *FaX_k, CLMEMVector *FbX_k, REAL8 dtauX, REAL8 dFreq, REAL8 FreqOut0, REAL8 dt_SRC, UINT4 size );

int XLALFFTisAvavible ( CLFFTType fft );
int XLALSelectFFT_OpenCL ( FFTfuncs *fftfuncs );

#ifdef HAVE_LIBCLFFT
static int
XLALComputeFFT_OpenCL ( void *plan,
                        CLMEMVector *in,
                        CLMEMVector *out
                      );

void *
XLALInititialFFT_Opencl ( UINT4 numSamplesFFT );

static void XLALDestroyCLFFTPlan ( void *plan );
#endif
#ifdef HAVE_LIBECLFFT
static int
XLALComputeECLFFT_OpenCL ( void *plan,
                           CLMEMVector *in,
                           CLMEMVector *out
                         );

void *
XLALInititialECLFFT_Opencl ( UINT4 numSamplesFFT );

static void XLALDestroyECLFFTPlan ( void *fftplan );
#endif


static int
XLALComputeDetectorFrameSample_OpenCL ( UINT4 numSFTsX,	REAL8 refTime8,	REAL8 Tsft,	REAL8 tStart_SRC_0,	REAL8 dt_SRC,	UINT4 numSamples_SRCX,
	REAL8 fHet, REAL8 tStart_DET_0,	ResampOpenCLWorkspace *ws, ResampMethodData *resamp, UINT4 X, const SSBtimes *SRCtimesX, const CLMEMVector *Timestamps_DETX );

static int
XLALSincInterpolateCLMEMTimeSeries ( CLMEMVector *y_out, const CLMEMVector *t_out, const COMPLEX8TimeSeries *ts_in, UINT4 Dterms );
// ==================== function definitions ====================

static void
XLALDestroyResampWorkspace_OpenCL ( void *workspace )
{
  ResampOpenCLWorkspace *ws = (ResampOpenCLWorkspace*) workspace;

  XLALDestroyCLMEMVector ( ws->TStmp1_SRC );
  XLALDestroyCLMEMVector ( ws->TStmp2_SRC );
  XLALDestroyCLMEMVector ( ws->SRCtimes_DET );

  XLALDestroyCLMEMVector ( ws->TS_FFT );

  XLALDestroyCLMEMVector ( ws->FaX_k );
  XLALDestroyCLMEMVector ( ws->FbX_k );
  XLALDestroyCLMEMVector ( ws->Fa_k );
  XLALDestroyCLMEMVector ( ws->Fb_k );
  XLALDestroyCLMEMVector ( ws->twoFPerDet );
  XLALDestroyCLMEMVector ( ws->fkdot );
  XLALDestroyCLMEMVector ( ws->a );
  XLALDestroyCLMEMVector ( ws->b );
  XLALDestroyCLMEMVector ( ws->DeltaT );
  XLALDestroyCLMEMVector ( ws->Tdot );

  XLALFree ( ws );
  return;

} // XLALDestroyResampWorkspace_OpenCL()

// ---------- internal functions ----------
static void
XLALDestroyResampMethodData_OpenCL ( void* method_data )
{

  ResampMethodData *resamp = (ResampMethodData*) method_data;
  FFTfuncs *fftfuncs = (FFTfuncs*) resamp->fftplan;

  XLALDestroyMultiCOMPLEX8TimeSeries_OpenCL (resamp->multiTimeSeries_DET );

  // ----- free buffer
  XLALDestroyMultiCOMPLEX8TimeSeries_OpenCL ( resamp->multiTimeSeries_SRC_a );
  XLALDestroyMultiCOMPLEX8TimeSeries_OpenCL ( resamp->multiTimeSeries_SRC_b );
  XLALDestroyMultiAMCoeffs ( resamp->multiAMcoef );
  XLALDestroyMultiSSBtimes ( resamp->multiSSBtimes );
  XLALDestroyMultiSSBtimes ( resamp->multiBinaryTimes );
  XLALDestroyMultiCLMEMVector ( resamp->MultiTimeStamps_GPU );

  (*fftfuncs->fftplan_destroy_funnc) ( fftfuncs->fftplan );
  XLALFree ( fftfuncs );

  if ( --kernel.refcount == 0 )
    {
      XLALPrintInfo( "%s: ResampOpenCL kernels reference count = %i, freeing ResampOpenCL kernels\n", __func__, kernel.refcount );
      cl_int err;
#ifdef HAVE_LIBCLFFT
      /* Release clFFT library. */
      XLAL_CHECK_VOID ( ( err = clfftTeardown()                                             ) == CL_SUCCESS ,XLAL_EFUNC, "Failed to release clFFT libary: %s", XLALGetOpenCLErrorString ( err ) );
#endif
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_DetectorFrameSample )       ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_SincInterpolate )           ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_ResampFabX_Raw )            ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_NormalizeFaFb )             ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_XLALComputeFstatFromFaFb )  ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseKernel ( kernel.kernel_ApplySpindownAndFreqShift ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Kernel: %s",       XLALGetOpenCLErrorString ( err ) );
      XLAL_CHECK_VOID ( ( err = clReleaseProgram ( kernel.resampProgramm )                  ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release Program: %s",      XLALGetOpenCLErrorString ( err ) );
      kernel.resampProgramm = 0;
      kernel.refcount = 0;
    } else {
      XLALPrintInfo( "%s: ResampOpenCL kernels reference count = %i\n", __func__, kernel.refcount );
    }
  XLALDestroyOpenCL_OBJ();

  XLALFree ( resamp );

} // XLALDestroyResampMethodData_OpenCL()

int
XLALSetupFstatResampOpenCL ( FstatCommon *common,
                             UINT4 numSamplesMax_SRC,
                             ResampMethodData *resamp,
                             FstatMethodFuncs *funcs
                           )
{
  XLAL_CHECK ( common != NULL, XLAL_EINVAL);
  XLAL_CHECK ( resamp != NULL, XLAL_EINVAL);
  XLAL_CHECK ( funcs != NULL, XLAL_EINVAL);

  ResampOpenCLWorkspace * ws =  (ResampOpenCLWorkspace*) common->workspace;
  UINT4 numSamplesFFT = resamp->numSamplesFFT;

  // Set method function pointers
  funcs->workspace_destroy_func = XLALDestroyResampWorkspace_OpenCL;
  funcs->compute_func = XLALComputeFstatResamp_OpenCL;
  funcs->method_data_destroy_func = XLALDestroyResampMethodData_OpenCL;

  OpenCL_OBJ *test = NULL;
  XLAL_CHECK ( ( test = XLALCreateOpenCL_OBJ ( ) ) != NULL, XLAL_EFUNC, "Fail to initialized OpenCL_OBJ");
  if (kernel.refcount == 0)
    {
      openclObjLocal = test;
      XLAL_CHECK ( XLALGetOpenCLProgramFromSource ( openCL_kernels, &kernel.resampProgramm) == XLAL_SUCCESS, XLAL_EFUNC );
      cl_int err;
      kernel.kernel_DetectorFrameSample = clCreateKernel ( kernel.resampProgramm, "detectorFrameSample", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the detectorFrameSample",  XLALGetOpenCLErrorString ( err ) );
      kernel.kernel_SincInterpolate = clCreateKernel ( kernel.resampProgramm, "XLALSincInterpolateCOMPLEX8TimeSeries", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the XLALSincInterpolateCOMPLEX8TimeSeries",  XLALGetOpenCLErrorString ( err ) );
      kernel.kernel_ApplySpindownAndFreqShift = clCreateKernel ( kernel.resampProgramm, "XLALApplySpindownAndFreqShift", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the XLALApplySpindownAndFreqShift",  XLALGetOpenCLErrorString ( err ) );
      kernel.kernel_ResampFabX_Raw = clCreateKernel ( kernel.resampProgramm, "resampFabX_Raw", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the ResampFabX_Raw",  XLALGetOpenCLErrorString ( err ) );
      kernel.kernel_NormalizeFaFb = clCreateKernel ( kernel.resampProgramm, "normalizeFaFb", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the normalizeFaFb",  XLALGetOpenCLErrorString ( err ) );
      kernel.kernel_XLALComputeFstatFromFaFb = clCreateKernel ( kernel.resampProgramm, "XLALComputeFstatFromFaFb", &err );
      XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Extract the kernel for the XLALComputeFstatFromFaFb",  XLALGetOpenCLErrorString ( err ) );
      kernel.refcount = 1;
    }
  else
    {
      XLAL_CHECK ( openclObjLocal == test, XLAL_EINVAL, "Something went wrong!" );
      kernel.refcount++;
    }
  UINT4 numDetectors = common->multiTimestamps->length;
  MultiCLMEMVector* multiTimestamps = NULL;
  XLAL_CHECK ( ( multiTimestamps = XLALCalloc ( 1, sizeof(MultiCLMEMVector) ) ) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( ( multiTimestamps->data = XLALCalloc ( numDetectors, sizeof(CLMEMVector*) ) ) != NULL, XLAL_ENOMEM );
  multiTimestamps->length = numDetectors;
  for ( UINT4 X = 0; X < numDetectors; X++ )
    {
      XLAL_CHECK ( (multiTimestamps->data[X] = XLALCreateCLMEMVector ( common->multiTimestamps->data[X]->length, sizeof(LIGOTimeGPS), CL_MEM_READ_ONLY )) != NULL, XLAL_EFUNC );
      XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( multiTimestamps->data[X], common->multiTimestamps->data[X]->data, common->multiTimestamps->data[X]->length, sizeof(LIGOTimeGPS), (0 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
    }
  resamp->MultiTimeStamps_GPU = multiTimestamps;
  XLAL_CHECK ( XLALTransferMultiCOMPLEX8TimeSeriesToCLMEM ( resamp->multiTimeSeries_DET ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALTransferMultiCOMPLEX8TimeSeriesToCLMEM ( resamp->multiTimeSeries_SRC_a ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALTransferMultiCOMPLEX8TimeSeriesToCLMEM ( resamp->multiTimeSeries_SRC_b ) == XLAL_SUCCESS, XLAL_EFUNC );

  // ---- re-use shared workspace, or allocate here ----------
  if ( ws != NULL )
    {
      if ( numSamplesFFT > ws->numSamplesFFTAlloc )
        {
          XLALDestroyCLMEMVector ( ws->TS_FFT );
          XLAL_CHECK ( (ws->TS_FFT = XLALCreateCLMEMVector ( numSamplesFFT, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
          ws->numSamplesFFTAlloc = numSamplesFFT;
        }

      // adjust maximal SRC-frame timeseries length, if necessary
      if ( numSamplesMax_SRC > ws->TStmp1_SRC->length ) {
          XLALDestroyCLMEMVector ( ws->TStmp1_SRC );
          XLALDestroyCLMEMVector ( ws->TStmp2_SRC );
          XLALDestroyCLMEMVector ( ws->SRCtimes_DET );
          XLAL_CHECK ( (ws->TStmp1_SRC = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
          XLAL_CHECK ( (ws->TStmp2_SRC = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
          XLAL_CHECK ( (ws->SRCtimes_DET = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(REAL8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      }

    } // end: if shared workspace given
  else
    {
      XLAL_CHECK ( (ws = XLALCalloc ( 1, sizeof(*ws))) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->TStmp1_SRC   = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->TStmp2_SRC   = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->SRCtimes_DET = XLALCreateCLMEMVector ( numSamplesMax_SRC, sizeof(REAL8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );

      XLAL_CHECK ( (ws->TS_FFT = XLALCreateCLMEMVector ( numSamplesFFT, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      ws->numSamplesFFTAlloc = numSamplesFFT;
      XLAL_CHECK ( ( ws->fkdot = XLALCreateCLMEMVector ( PULSAR_MAX_SPINS, sizeof(COMPLEX8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->a = XLALCreateCLMEMVector ( 1, sizeof(REAL4), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->b = XLALCreateCLMEMVector ( 1, sizeof(REAL4), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->Tdot = XLALCreateCLMEMVector ( 1, sizeof(REAL8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->DeltaT = XLALCreateCLMEMVector ( 1, sizeof(REAL8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );

      common->workspace = ws;
    } // end: if we create our own workspace

  FFTfuncs *fftfuncs;
  XLAL_CHECK ( ( fftfuncs = XLALCalloc (1,sizeof(FFTfuncs))) != NULL, XLAL_ENOMEM );

  XLAL_CHECK ( XLALSelectFFT_OpenCL( fftfuncs) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( (fftfuncs->fftplan = (*fftfuncs->initialfft_func) ( numSamplesFFT )) != NULL, XLAL_ENOMEM );
  resamp->fftplan = fftfuncs;

  return XLAL_SUCCESS;
} // XLALSetupFstatResampOpenCL()

static int
XLALComputeFstatResamp_OpenCL ( FstatResults* Fstats,
                         const FstatCommon *common,
                         void *method_data
                       )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  // Check input
  XLAL_CHECK(Fstats != NULL, XLAL_EFAULT);
  XLAL_CHECK(common != NULL, XLAL_EFAULT);
  XLAL_CHECK(method_data != NULL, XLAL_EFAULT);

  ResampMethodData *resamp = (ResampMethodData*) method_data;

  const FstatQuantities whatToCompute = Fstats->whatWasComputed;
  XLAL_CHECK ( !(whatToCompute & FSTATQ_ATOMS_PER_DET), XLAL_EINVAL, "Resampling does not currently support atoms per detector" );

  ResampOpenCLWorkspace *ws = (ResampOpenCLWorkspace*) common->workspace;

  // ----- handy shortcuts ----------
  PulsarDopplerParams thisPoint = Fstats->doppler;
  const MultiCOMPLEX8TimeSeries *multiTimeSeries_DET = resamp->multiTimeSeries_DET;
  UINT4 numDetectors = multiTimeSeries_DET->length;

	XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( ws->fkdot, thisPoint.fkdot, PULSAR_MAX_SPINS, sizeof(thisPoint.fkdot[0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  // collect internal timing info
  BOOLEAN collectTiming = resamp->collectTiming;
  Timings_t *Tau = &(resamp->timingResamp.Tau);
  XLAL_INIT_MEM ( (*Tau) );	// these need to be initialized to 0 for each call

  REAL8 ticStart = 0, tocEnd = 0;
  REAL8 tic = 0, toc = 0;
  if ( collectTiming ) {
    XLAL_INIT_MEM ( (*Tau) );	// re-set all timings to 0 at beginning of each Fstat-call
    ticStart = XLALGetCPUTime();
  }
  // Note: all buffering is done within that function
  XLAL_CHECK ( XLALBarycentricResampleMultiCOMPLEX8TimeSeries ( resamp, &thisPoint, common ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( whatToCompute == FSTATQ_NONE ) {
    return XLAL_SUCCESS;
  }

  MultiCOMPLEX8TimeSeries *multiTimeSeries_SRC_a = resamp->multiTimeSeries_SRC_a;
  MultiCOMPLEX8TimeSeries *multiTimeSeries_SRC_b = resamp->multiTimeSeries_SRC_b;

  // ============================== check workspace is properly allocated and initialized ===========

  // ----- workspace that depends on maximal number of output frequency bins 'numFreqBins' ----------
  UINT4 numFreqBins = Fstats->numFreqBins;

  if ( collectTiming ) {
    tic = XLALGetCPUTime();
  }

  if ( numFreqBins > ws->numFreqBinsAlloc )
    {
      XLALDestroyCLMEMVector ( ws->Fa_k );
      XLALDestroyCLMEMVector ( ws->Fb_k );
      XLALDestroyCLMEMVector ( ws->twoFPerDet );
      XLAL_CHECK ( (ws->Fa_k = XLALCreateCLMEMVector ( numFreqBins, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->Fb_k = XLALCreateCLMEMVector ( numFreqBins, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->twoFPerDet = XLALCreateCLMEMVector ( numFreqBins, sizeof(REAL4), CL_MEM_WRITE_ONLY )) != NULL, XLAL_ENOMEM );
      XLALDestroyCLMEMVector ( ws->FaX_k );
      XLALDestroyCLMEMVector ( ws->FbX_k );
      XLAL_CHECK ( (ws->FaX_k = XLALCreateCLMEMVector ( numFreqBins, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( (ws->FbX_k = XLALCreateCLMEMVector ( numFreqBins, sizeof(COMPLEX8), CL_MEM_READ_WRITE )) != NULL, XLAL_ENOMEM );
      ws->numFreqBinsAlloc = numFreqBins;	// keep track of allocated array length
  }

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    Tau->Mem = (toc-tic);	// this one doesn't scale with number of detector!
  }
  // ====================================================================================================

  // loop over detectors
  for ( UINT4 X=0; X < numDetectors; X++ )
    {
      const COMPLEX8TimeSeries *TimeSeriesX_SRC_a = multiTimeSeries_SRC_a->data[X];
      const COMPLEX8TimeSeries *TimeSeriesX_SRC_b = multiTimeSeries_SRC_b->data[X];

      // compute {Fa^X(f_k), Fb^X(f_k)}: results returned via workspace ws
      XLAL_CHECK ( XLALComputeFaFb_Resamp_OpenCL ( resamp, ws, thisPoint, common->dFreq, numFreqBins, TimeSeriesX_SRC_a, TimeSeriesX_SRC_b ) == XLAL_SUCCESS, XLAL_EFUNC );

      if ( collectTiming ) {
        tic = XLALGetCPUTime();
      }
      if ( X == 0 )
        { // avoid having to memset this array: for the first detector we *copy* results
          XLAL_CHECK ( XLALCopyCLMEMVector( ws->Fa_k, ws->FaX_k, numFreqBins, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
          XLAL_CHECK ( XLALCopyCLMEMVector( ws->Fb_k, ws->FbX_k, numFreqBins, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );
        } // end: if X==0
      else
        { // for subsequent detectors we *add to* them
          XLAL_CHECK ( XLALAddCLMEMVectorCOMPLEX8(ws->Fa_k, ws->Fa_k, ws->FaX_k, numFreqBins, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
          XLAL_CHECK ( XLALAddCLMEMVectorCOMPLEX8(ws->Fb_k, ws->Fb_k, ws->FbX_k, numFreqBins, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );
        } // end:if X>0

      if ( collectTiming ) {
        toc = XLALGetCPUTime();
        Tau->SumFabX += (toc-tic);
        tic = toc;
      }

      // ----- if requested: compute per-detector Fstat_X_k
      if ( whatToCompute & FSTATQ_2F_PER_DET )
        {
          const REAL4 AdX = resamp->MmunuX[X].Ad;
          const REAL4 BdX = resamp->MmunuX[X].Bd;
          const REAL4 CdX = resamp->MmunuX[X].Cd;
          const REAL4 EdX = resamp->MmunuX[X].Ed;
          const REAL4 DdX_inv = 1.0f / resamp->MmunuX[X].Dd;
          XLAL_CHECK ( XLALComputeFstatFromFaFb_OpenCL (ws->twoFPerDet, ws->FaX_k, ws->FbX_k, AdX, BdX, CdX, EdX, DdX_inv,numFreqBins ) == XLAL_SUCCESS, XLAL_EFUNC );
          XLAL_CHECK ( XLALGetVectorFromCLMEMVector ( Fstats->twoFPerDet[X], ws->twoFPerDet, numFreqBins, sizeof(Fstats->twoFPerDet[X][0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
        } // end: if compute F_X

      if ( collectTiming ) {
        toc = XLALGetCPUTime();
        Tau->Fab2F += ( toc - tic );
      }

    } // for X < numDetectors

  if ( collectTiming ) {
    Tau->SumFabX /= numDetectors;
    Tau->Fab2F /= numDetectors;
    tic = XLALGetCPUTime();
  }

  if ( whatToCompute & FSTATQ_2F )
    {
      const REAL4 Ad = resamp->Mmunu.Ad;
      const REAL4 Bd = resamp->Mmunu.Bd;
      const REAL4 Cd = resamp->Mmunu.Cd;
      const REAL4 Ed = resamp->Mmunu.Ed;
      const REAL4 Dd_inv = 1.0f / resamp->Mmunu.Dd;
      XLAL_CHECK ( XLALComputeFstatFromFaFb_OpenCL(ws->twoFPerDet,ws->Fa_k,ws->Fb_k,Ad,Bd,Cd,Ed,Dd_inv,numFreqBins) == XLAL_SUCCESS, XLAL_EFUNC );
      XLAL_CHECK ( XLALGetVectorFromCLMEMVector ( Fstats->twoF, ws->twoFPerDet, numFreqBins, sizeof(Fstats->twoF[0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );


    if ( whatToCompute & FSTATQ_FAFB )
      {
        XLAL_CHECK ( XLALGetVectorFromCLMEMVector ( Fstats->Fa, ws->Fa_k, numFreqBins, sizeof(Fstats->Fa[0]), (0 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
        XLAL_CHECK ( XLALGetVectorFromCLMEMVector ( Fstats->Fb, ws->Fb_k, numFreqBins, sizeof(Fstats->Fb[0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
      }

    } // if FSTATQ_2F

  if ( collectTiming ) {
      toc = XLALGetCPUTime();
      Tau->Fab2F += ( toc - tic );
  }

  // Return F-atoms per detector
  if (whatToCompute & FSTATQ_ATOMS_PER_DET) {
    XLAL_ERROR(XLAL_EFAILED, "NOT implemented!");
  }

  // Return antenna-pattern matrix
  Fstats->Mmunu = resamp->Mmunu;

  // return per-detector antenna-pattern matrices
  for ( UINT4 X = 0; X < numDetectors; X ++ )
    {
      Fstats->MmunuX[X] = resamp->MmunuX[X];
    }

  if ( collectTiming )
    {
      tocEnd = XLALGetCPUTime();

      FstatTimingGeneric *tiGen = &(resamp->timingGeneric);
      FstatTimingResamp  *tiRS  = &(resamp->timingResamp);
      XLAL_CHECK ( numDetectors == tiGen->Ndet, XLAL_EINVAL, "Inconsistent number of detectors between XLALCreateSetup() [%d] and XLALComputeFstat() [%d]\n", tiGen->Ndet, numDetectors );

      Tau->Total = (tocEnd - ticStart);
      // rescale all relevant timings to per-detector
      Tau->Total /= numDetectors;
      Tau->Bary  /= numDetectors;
      Tau->Spin  /= numDetectors;
      Tau->FFT   /= numDetectors;
      Tau->Norm  /= numDetectors;
      Tau->Copy  /= numDetectors;
      REAL8 Tau_buffer = Tau->Bary;
      // compute generic F-stat timing model contributions
      UINT4 NFbin      = Fstats->numFreqBins;
      REAL8 tauF_eff   = Tau->Total / NFbin;
      REAL8 tauF_core  = (Tau->Total - Tau_buffer) / NFbin;

      // compute resampling timing model coefficients
      REAL8 tau0_Fbin  = (Tau->Copy + Tau->Norm + Tau->SumFabX + Tau->Fab2F) / NFbin;
      REAL8 tau0_spin  = Tau->Spin / (tiRS->Resolution * tiRS->NsampFFT );
      REAL8 tau0_FFT   = Tau->FFT / (5.0 * tiRS->NsampFFT * log2(tiRS->NsampFFT));

      // update the averaged timing-model quantities
      tiGen->NCalls ++;	// keep track of number of Fstat-calls for timing
#define updateAvgF(q) tiGen->q = ((tiGen->q *(tiGen->NCalls-1) + q)/(tiGen->NCalls))
      updateAvgF(tauF_eff);
      updateAvgF(tauF_core);
      // we also average NFbin, which can be different between different calls to XLALComputeFstat() (contrary to Ndet)
      updateAvgF(NFbin);

#define updateAvgRS(q) tiRS->q = ((tiRS->q *(tiGen->NCalls-1) + q)/(tiGen->NCalls))
      updateAvgRS(tau0_Fbin);
      updateAvgRS(tau0_spin);
      updateAvgRS(tau0_FFT);

      // buffer-quantities only updated if buffer was actually recomputed
      if ( Tau->BufferRecomputed )
        {
          REAL8 tau0_bary   = Tau_buffer / (tiRS->Resolution * tiRS->NsampFFT);
          REAL8 tauF_buffer = Tau_buffer / NFbin;

          updateAvgF(tauF_buffer);
          updateAvgRS(tau0_bary);
        } // if BufferRecomputed

    } // if collectTiming

  return XLAL_SUCCESS;

} // XLALComputeFstatResamp_OpenCL()


static int
XLALComputeFaFb_Resamp_OpenCL ( ResampMethodData *resamp,				//!< [in,out] buffered resampling data and workspace
                                ResampOpenCLWorkspace *ws,					//!< [in,out] resampling workspace (memory-sharing across segments)
                                const PulsarDopplerParams thisPoint,			//!< [in] Doppler point to compute {FaX,FbX} for
                                REAL8 dFreq,						//!< [in] output frequency resolution
                                UINT4 numFreqBins,					//!< [in] number of output frequency bins
                                const COMPLEX8TimeSeries * restrict TimeSeries_SRC_a,	//!< [in] SRC-frame single-IFO timeseries * a(t)
                                const COMPLEX8TimeSeries * restrict TimeSeries_SRC_b	//!< [in] SRC-frame single-IFO timeseries * b(t)
                         )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( (resamp != NULL) && (ws != NULL) && (TimeSeries_SRC_a != NULL) && (TimeSeries_SRC_b != NULL), XLAL_EINVAL );
  XLAL_CHECK ( dFreq > 0, XLAL_EINVAL );
  XLAL_CHECK ( numFreqBins <= ws->numFreqBinsAlloc, XLAL_EINVAL );

  REAL8 FreqOut0 = thisPoint.fkdot[0];

  // compute frequency shift to align heterodyne frequency with output frequency bins
  REAL8 fHet   = TimeSeries_SRC_a->f0;
  REAL8 dt_SRC = TimeSeries_SRC_a->deltaT;

  REAL8 dFreqFFT = dFreq / resamp->decimateFFT;	// internally may be using higher frequency resolution dFreqFFT than requested
  REAL8 freqShift = remainder ( FreqOut0 - fHet, dFreq ); // frequency shift to closest bin
  REAL8 fMinFFT = fHet + freqShift - dFreqFFT * (resamp->numSamplesFFT/2);	// we'll shift DC into the *middle bin* N/2  [N always even!]
  XLAL_CHECK ( FreqOut0 >= fMinFFT, XLAL_EDOM, "Lowest output frequency outside the available frequency band: [FreqOut0 = %.16g] < [fMinFFT = %.16g]\n", FreqOut0, fMinFFT );
  UINT4 offset_bins = (UINT4) lround ( ( FreqOut0 - fMinFFT ) / dFreqFFT );
  UINT4 maxOutputBin = offset_bins + (numFreqBins - 1) * resamp->decimateFFT;
  XLAL_CHECK ( maxOutputBin < resamp->numSamplesFFT, XLAL_EDOM, "Highest output frequency bin outside available band: [maxOutputBin = %d] >= [numSamplesFFT = %d]\n", maxOutputBin, resamp->numSamplesFFT );

  FstatTimingResamp *tiRS = &(resamp->timingResamp);
  BOOLEAN collectTiming = resamp->collectTiming;
  REAL8 tic = 0, toc = 0;

  FFTfuncs *fftfuncs = (FFTfuncs*) resamp->fftplan;

  const CLMEMSequence *TimeSeries_sequence_SRCX_a = (const CLMEMSequence*) TimeSeries_SRC_a->data;
  const CLMEMSequence *TimeSeries_sequence_SRCX_b = (const CLMEMSequence*) TimeSeries_SRC_b->data;
  XLAL_CHECK ( resamp->numSamplesFFT >= TimeSeries_sequence_SRCX_a->length, XLAL_EFAILED, "[numSamplesFFT = %d] < [len(TimeSeries_SRC_a) = %d]\n", resamp->numSamplesFFT, TimeSeries_sequence_SRCX_a->length );
  XLAL_CHECK ( resamp->numSamplesFFT >= TimeSeries_sequence_SRCX_b->length, XLAL_EFAILED, "[numSamplesFFT = %d] < [len(TimeSeries_SRC_b) = %d]\n", resamp->numSamplesFFT, TimeSeries_sequence_SRCX_b->length );

  if ( collectTiming ) {
    tic = XLALGetCPUTime();
  }
  XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( ws->TS_FFT, 0, (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
  // ----- compute FaX_k
  // apply spindown phase-factors, store result in zero-padded timeseries for 'FFT'ing
  XLAL_CHECK ( XLALApplySpindownAndFreqShift_OpenCL ( ws->TS_FFT, TimeSeries_SRC_a, &thisPoint, freqShift, ws->fkdot ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.Spin += ( toc - tic);
    tic = toc;
  }

  // Fourier transform the resampled Fa(t)
  XLAL_CHECK ( (*fftfuncs->computefft_func) ( fftfuncs->fftplan, ws->TS_FFT, NULL ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.FFT += ( toc - tic);
    tic = toc;
  }

  XLAL_CHECK ( XLALResampFabX_Raw_OpenCL ( ws->FaX_k, ws->TS_FFT, offset_bins, resamp->decimateFFT, numFreqBins ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.Copy += ( toc - tic);
    tic = toc;
  }

  // ----- compute FbX_k
  // apply spindown phase-factors, store result in zero-padded timeseries for 'FFT'ing
  // because of inPlace FFT, we need to clear the buffer
  XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( ws->TS_FFT, 0, (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALApplySpindownAndFreqShift_OpenCL ( ws->TS_FFT, TimeSeries_SRC_b, &thisPoint, freqShift, ws->fkdot ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.Spin += ( toc - tic);
    tic = toc;
  }

  // Fourier transform the resampled Fa(t)
  XLAL_CHECK ( (*fftfuncs->computefft_func) ( fftfuncs->fftplan, ws->TS_FFT, NULL ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.FFT += ( toc - tic);
    tic = toc;
  }

  XLAL_CHECK ( XLALResampFabX_Raw_OpenCL ( ws->FbX_k, ws->TS_FFT, offset_bins, resamp->decimateFFT, numFreqBins ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.Copy += ( toc - tic);
    tic = toc;
  }

  // ----- normalization factors to be applied to Fa and Fb:
  const REAL8 dtauX = XLALGPSDiff ( &(TimeSeries_SRC_a->epoch), &(thisPoint.refTime) );
  XLAL_CHECK ( XLALNormalizeFaFb_OpenCL ( ws->FaX_k, ws->FbX_k, dtauX,dFreq, FreqOut0, dt_SRC, numFreqBins ) == XLAL_SUCCESS, XLAL_EFUNC );

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    tiRS->Tau.Norm += ( toc - tic);
    tic = toc;
  }

  return XLAL_SUCCESS;

} // XLALComputeFaFb_Resamp_OpenCL()

static int
XLALApplySpindownAndFreqShift_OpenCL ( CLMEMVector *restrict xOut,      			///< [out] the spindown-corrected SRC-frame timeseries
                                       const COMPLEX8TimeSeries *restrict xIn,		///< [in] the input SRC-frame timeseries
                                       const PulsarDopplerParams *restrict doppler,	///< [in] containing spindown parameters
                                       REAL8 freqShift,					///< [in] frequency-shift to apply, sign is "new - old"
                                       CLMEMVector *fkdot ) ///< [in] fkdot on GPU Memory
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  // input sanity checks
  XLAL_CHECK ( xOut != NULL, XLAL_EINVAL );
  XLAL_CHECK ( xIn != NULL, XLAL_EINVAL );
  XLAL_CHECK ( doppler != NULL, XLAL_EINVAL );
  XLAL_CHECK ( fkdot != NULL, XLAL_EINVAL );
  const CLMEMSequence *xIn_sequence = (const CLMEMSequence*) xIn->data;

  // determine number of spin downs to include
  UINT4 s_max = PULSAR_MAX_SPINS - 1;
  while ( (s_max > 0) && (doppler->fkdot[s_max] == 0) ) {
    s_max --;
  }

  REAL8 dt = xIn->deltaT;
  UINT4 numSamplesIn  = xIn_sequence->length;

  LIGOTimeGPS epoch = xIn->epoch;
  REAL8 Dtau0 = XLALGPSDiff ( &epoch, &(doppler->refTime) );

  cl_int err;
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 0, sizeof(xOut->data),         (void*) &(xOut->data)               ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 1, sizeof(xIn_sequence->data), (const void*) &(xIn_sequence->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 2, sizeof(fkdot->data),        (void*) &(fkdot->data)              ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 3, sizeof(dt),                 (void*) &dt                         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 4, sizeof(Dtau0),              (void*) &Dtau0                      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 5, sizeof(freqShift),          (void*) &freqShift                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 6, sizeof(s_max),              (void*) &s_max                      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ApplySpindownAndFreqShift, 7, sizeof(numSamplesIn),       (void*) &numSamplesIn               ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_ApplySpindownAndFreqShift, xOut->length, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;

} // XLALApplySpindownAndFreqShift()

int
XLALBarycentricResampleMultiCOMPLEX8TimeSeries_OPENCL ( ResampMethodData *resamp,		// [in/out] resampling input and buffer (to store resampling TS)
                                                        const FstatCommon *common,		// [in] various input quantities and parameters used here
                                                        MultiSSBtimes *multiSRCtimes
                                                      )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  // check input sanity
  XLAL_CHECK ( common != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp != NULL, XLAL_EINVAL );
  XLAL_CHECK ( multiSRCtimes != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_DET != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_a != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_b != NULL, XLAL_EINVAL );

  UINT4 numDetectors = resamp->multiTimeSeries_DET->length;
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_a->length == numDetectors, XLAL_EINVAL, "Inconsistent number of detectors tsDET(%d) != tsSRC(%d)\n", numDetectors, resamp->multiTimeSeries_SRC_a->length );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_b->length == numDetectors, XLAL_EINVAL, "Inconsistent number of detectors tsDET(%d) != tsSRC(%d)\n", numDetectors, resamp->multiTimeSeries_SRC_b->length );

  ResampOpenCLWorkspace *ws = (ResampOpenCLWorkspace*) common->workspace;

  // shorthands
  REAL8 fHet = resamp->multiTimeSeries_DET->data[0]->f0;
  REAL8 Tsft = common->multiTimestamps->data[0]->deltaT;
  REAL8 dt_SRC = resamp->multiTimeSeries_SRC_a->data[0]->deltaT;
  const MultiCLMEMVector *multiTimestamps = (MultiCLMEMVector*) resamp->MultiTimeStamps_GPU;

  // loop over detectors X
  for ( UINT4 X = 0; X < numDetectors; X++)
    {
      // shorthand pointers: input
      const COMPLEX8TimeSeries *TimeSeries_DETX = resamp->multiTimeSeries_DET->data[X];
      const CLMEMVector *Timestamps_DETX =  multiTimestamps->data[X];
      const SSBtimes *SRCtimesX                 = multiSRCtimes->data[X];

      // shorthand pointers: output
      COMPLEX8TimeSeries *TimeSeries_SRCX_a     = resamp->multiTimeSeries_SRC_a->data[X];
      COMPLEX8TimeSeries *TimeSeries_SRCX_b     = resamp->multiTimeSeries_SRC_b->data[X];
      CLMEMSequence *TimeSeries_sequence_SRCX_a     = (CLMEMSequence*) resamp->multiTimeSeries_SRC_a->data[X]->data;
      CLMEMSequence *TimeSeries_sequence_SRCX_b     = (CLMEMSequence*) resamp->multiTimeSeries_SRC_b->data[X]->data;
      CLMEMSequence *TimeSeries_sequence_DETX     = (CLMEMSequence*) resamp->multiTimeSeries_DET->data[X]->data;
      CLMEMVector *ti_DET = ws->SRCtimes_DET;

      // useful shorthands
      REAL8 refTime8        = XLALGPSGetREAL8 ( &SRCtimesX->refTime );
      UINT4 numSFTsX        = Timestamps_DETX->length;
      UINT4 numSamples_DETX = TimeSeries_sequence_DETX->length;
      UINT4 numSamples_SRCX = TimeSeries_sequence_SRCX_a->length;

      // sanity checks on input data
      XLAL_CHECK ( numSamples_SRCX == TimeSeries_sequence_SRCX_b->length, XLAL_EINVAL );
      XLAL_CHECK ( dt_SRC == TimeSeries_SRCX_a->deltaT, XLAL_EINVAL );
      XLAL_CHECK ( dt_SRC == TimeSeries_SRCX_b->deltaT, XLAL_EINVAL );
      XLAL_CHECK ( numSamples_DETX > 0, XLAL_EINVAL, "Input timeseries for detector X=%d has zero samples. Can't handle that!\n", X );
      XLAL_CHECK ( (SRCtimesX->DeltaT->length == numSFTsX) && (SRCtimesX->Tdot->length == numSFTsX), XLAL_EINVAL );
      REAL8 fHetX = resamp->multiTimeSeries_DET->data[X]->f0;
      XLAL_CHECK ( fabs( fHet - fHetX ) < LAL_REAL8_EPS * fHet, XLAL_EINVAL, "Input timeseries must have identical heterodyning frequency 'f0(X=%d)' (%.16g != %.16g)\n", X, fHet, fHetX );
      REAL8 TsftX = common->multiTimestamps->data[X]->deltaT;
      XLAL_CHECK ( Tsft == TsftX, XLAL_EINVAL, "Input timestamps must have identical stepsize 'Tsft(X=%d)' (%.16g != %.16g)\n", X, Tsft, TsftX );

      TimeSeries_SRCX_a->f0 = fHet;
      TimeSeries_SRCX_b->f0 = fHet;
      // set SRC-frame time-series start-time
      REAL8 tStart_SRC_0 = refTime8 + SRCtimesX->DeltaT->data[0] - (0.5*Tsft) * SRCtimesX->Tdot->data[0];
      LIGOTimeGPS epoch;
      XLALGPSSetREAL8 ( &epoch, tStart_SRC_0 );
      TimeSeries_SRCX_a->epoch = epoch;
      TimeSeries_SRCX_b->epoch = epoch;

      // make sure all output samples are initialized to zero first, in case of gaps
      XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( TimeSeries_sequence_SRCX_a, 0, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
      XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( TimeSeries_sequence_SRCX_b, 0, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
      // make sure detector-frame timesteps to interpolate to are initialized to 0, in case of gaps
      XLAL_CHECK ( XLALMemsetCLMEMVectorREAL8 ( ws->SRCtimes_DET, 0, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
      XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( ws->TStmp1_SRC, 0, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );
      XLAL_CHECK ( XLALMemsetCLMEMVectorCOMPLEX8 ( ws->TStmp2_SRC, 0, (1==0) ) == XLAL_SUCCESS, XLAL_EFUNC );

      REAL8 tStart_DET_0 = XLALGPSGetREAL8 ( &(common->multiTimestamps->data[X]->data[0]) );// START time of the SFT at the detector

      // loop over SFT timestamps and compute the detector frame time samples corresponding to uniformly sampled SRC time samples
      XLAL_CHECK ( XLALComputeDetectorFrameSample_OpenCL( numSFTsX, refTime8, Tsft, tStart_SRC_0, dt_SRC, numSamples_SRCX, fHet, tStart_DET_0, ws, resamp, X, SRCtimesX, (const CLMEMVector*) Timestamps_DETX ) == XLAL_SUCCESS, XLAL_EFUNC );

      XLAL_CHECK ( ti_DET->length >= TimeSeries_sequence_SRCX_a->length, XLAL_EINVAL );
      UINT4 bak_length = ti_DET->length;
      ti_DET->length = TimeSeries_sequence_SRCX_a->length;
      XLAL_CHECK ( XLALSincInterpolateCLMEMTimeSeries( TimeSeries_sequence_SRCX_a, ti_DET, TimeSeries_DETX, resamp->Dterms ) == XLAL_SUCCESS, XLAL_EFUNC );
      ti_DET->length = bak_length;

      // apply heterodyne correction and AM-functions a(t) and b(t) to interpolated timeseries
      XLAL_CHECK ( XLALMultiplyCLMEMVectorCOMPLEX8 ( TimeSeries_sequence_SRCX_b, TimeSeries_sequence_SRCX_a, ws->TStmp2_SRC, numSamples_SRCX, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );
      XLAL_CHECK ( XLALMultiplyCLMEMVectorCOMPLEX8 ( TimeSeries_sequence_SRCX_a, TimeSeries_sequence_SRCX_a, ws->TStmp1_SRC, numSamples_SRCX, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );

    } // for X < numDetectors

  return XLAL_SUCCESS;

} // XLALBarycentricResampleMultiCOMPLEX8TimeSeries()

static int
XLALComputeDetectorFrameSample_OpenCL ( UINT4 numSFTsX,
                                        REAL8 refTime8,
                                        REAL8 Tsft,
                                        REAL8 tStart_SRC_0,
                                        REAL8 dt_SRC,
                                        UINT4 numSamples_SRCX,
                                        REAL8 fHet,
                                        REAL8 tStart_DET_0,
                                        ResampOpenCLWorkspace *ws,
                                        ResampMethodData *resamp,
                                        UINT4 X,
                                        const SSBtimes *SRCtimesX,
                                        const CLMEMVector *Timestamps_DETX
                           )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( SRCtimesX != NULL, XLAL_EINVAL );
  XLAL_CHECK ( Timestamps_DETX != NULL, XLAL_EINVAL );
  XLAL_CHECK ( ws != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp != NULL, XLAL_EINVAL );

  // compute max numSamplesSFT_SRC_al for this detector
   UINT4 subwork_size = 0;
   for ( UINT4 alpha = 0; alpha < numSFTsX; alpha ++ )
      {
        // define some useful shorthands
        REAL8 Tdot_al       = SRCtimesX->Tdot->data [ alpha ];		// the instantaneous time derivitive dt_SRC/dt_DET at the MID-POINT of the SFT
        REAL8 tMid_SRC_al   = refTime8 + SRCtimesX->DeltaT->data[alpha];	// MID-POINT time of the SFT at the SRC
        REAL8 tStart_SRC_al = tMid_SRC_al - 0.5 * Tsft * Tdot_al;		// approximate START time of the SFT at the SRC
        REAL8 tEnd_SRC_al   = tMid_SRC_al + 0.5 * Tsft * Tdot_al;		// approximate END time of the SFT at the SRC

        // indices of first and last SRC-frame sample corresponding to this SFT
        UINT4 iStart_SRC_al = ceil ( (tStart_SRC_al - tStart_SRC_0) / dt_SRC );	// the index of the resampled timeseries corresponding to the start of the SFT
        UINT4 iEnd_SRC_al   = floor ( (tEnd_SRC_al - tStart_SRC_0) / dt_SRC );	// the index of the resampled timeseries corresponding to the end of the SFT

        // truncate to actual SRC-frame timeseries
        iStart_SRC_al = MYMIN ( iStart_SRC_al, numSamples_SRCX - 1);
        iEnd_SRC_al   = MYMIN ( iEnd_SRC_al, numSamples_SRCX - 1);
        subwork_size = MYMAX(subwork_size, iEnd_SRC_al - iStart_SRC_al + 1);
        }
  const UINT4 size = subwork_size*numSFTsX;
  cl_int err;

  if ( SRCtimesX->Tdot->length > ws->Tdot->length )
    {
      XLALDestroyCLMEMVector ( ws->a );
      XLALDestroyCLMEMVector ( ws->b );
      XLALDestroyCLMEMVector ( ws->DeltaT );
      XLALDestroyCLMEMVector ( ws->Tdot );
      XLAL_CHECK ( ( ws->Tdot = XLALCreateCLMEMVector ( SRCtimesX->Tdot->length, sizeof(SRCtimesX->Tdot->data[0]), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->DeltaT = XLALCreateCLMEMVector ( SRCtimesX->DeltaT->length, sizeof(SRCtimesX->DeltaT->data[0]), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->b = XLALCreateCLMEMVector ( resamp->multiAMcoef->data[X]->b->length, sizeof(resamp->multiAMcoef->data[X]->b->data[0]), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
      XLAL_CHECK ( ( ws->a = XLALCreateCLMEMVector ( resamp->multiAMcoef->data[X]->a->length, sizeof(resamp->multiAMcoef->data[X]->a->data[0]), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
    }
  XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( ws->Tdot, SRCtimesX->Tdot->data, SRCtimesX->Tdot->length, sizeof(SRCtimesX->Tdot->data[0]), (1 == 0) ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( ws->b, resamp->multiAMcoef->data[X]->b->data, resamp->multiAMcoef->data[X]->b->length, sizeof(resamp->multiAMcoef->data[X]->b->data[0]), (1 == 0) ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( ws->a, resamp->multiAMcoef->data[X]->a->data, resamp->multiAMcoef->data[X]->a->length, sizeof(resamp->multiAMcoef->data[X]->a->data[0]), (1 == 0) ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( ws->DeltaT, SRCtimesX->DeltaT->data, SRCtimesX->DeltaT->length, sizeof(SRCtimesX->DeltaT->data[0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 0, sizeof(subwork_size),            (void*) &subwork_size                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 1, sizeof(refTime8),                (void*) &refTime8                      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 2, sizeof(Tsft),                    (void*) &Tsft                          ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 3, sizeof(tStart_SRC_0),            (void*) &tStart_SRC_0                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 4, sizeof(dt_SRC),                  (void*) &dt_SRC                        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 5, sizeof(numSamples_SRCX),         (void*) &numSamples_SRCX               ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 6, sizeof(fHet),                    (void*) &fHet                          ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 7, sizeof(tStart_DET_0),            (void*) &tStart_DET_0                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 8, sizeof(ws->Tdot->data),          (void*) &(ws->Tdot->data)              ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 9, sizeof(ws->DeltaT->data),        (void*) &(ws->DeltaT->data)            ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 10, sizeof(Timestamps_DETX->data),  (const void*) &(Timestamps_DETX->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 11, sizeof(ws->a->data),            (void*) &(ws->a->data)                 ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 12, sizeof(ws->b->data),            (void*) &(ws->b->data)                 ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 13, sizeof(ws->SRCtimes_DET->data), (void*) &(ws->SRCtimes_DET->data)      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 14, sizeof(ws->TStmp1_SRC->data),   (void*) &(ws->TStmp1_SRC->data)        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 15, sizeof(ws->TStmp2_SRC->data),   (void*) &(ws->TStmp2_SRC->data)        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_DetectorFrameSample, 16, sizeof(numSFTsX),               (void*) &numSFTsX                      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_DetectorFrameSample, size, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;
} // XLALComputeDetectorFrameSample_OpenCL()

static int
XLALSincInterpolateCLMEMTimeSeries ( CLMEMVector *y_out,
                                     const CLMEMVector *t_out,
                                     const COMPLEX8TimeSeries *ts_in,
                                     UINT4 Dterms
                                   )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( y_out != NULL, XLAL_EINVAL );
  XLAL_CHECK ( t_out != NULL, XLAL_EINVAL );
  XLAL_CHECK ( ts_in != NULL, XLAL_EINVAL );
  XLAL_CHECK ( y_out->length == t_out->length, XLAL_EINVAL );

  UINT4 numSamplesOut = t_out->length;
  const CLMEMSequence *sequence_in = (const CLMEMSequence*) ts_in->data;
  UINT4 numSamplesIn = sequence_in->length;
  REAL8 dt = ts_in->deltaT;
  REAL8 tmin = XLALGPSGetREAL8 ( &(ts_in->epoch) );

  REAL8Window *win;
  UINT4 winLen = 2 * Dterms + 1;
  XLAL_CHECK ( (win = XLALCreateHammingREAL8Window ( winLen )) != NULL, XLAL_EFUNC );
  CLMEMVector *CLMEMwin;
  XLAL_CHECK ( ( CLMEMwin = XLALCreateCLMEMVector ( win->data->length, sizeof(REAL8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( XLALTransferVectorToCLMEMVector ( CLMEMwin, win->data->data, win->data->length, sizeof(win->data->data[0]), (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  const REAL8 oodt = 1.0 / dt;
  cl_int err;
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 0, sizeof(y_out->data),       (void*) &(y_out->data)             ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 1, sizeof(t_out->data),       (const void*) &(t_out->data)       ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 2, sizeof(sequence_in->data), (const void*) &(sequence_in->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 3, sizeof(Dterms),            (void*) &Dterms                    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 4, sizeof(numSamplesIn),      (void*) &numSamplesIn              ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 5, sizeof(dt),                (void*) &dt                        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 6, sizeof(oodt),              (const void*) &oodt                ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 7, sizeof(tmin),              (void*) &tmin                      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 8, sizeof(CLMEMwin->data),    (const void*) &(CLMEMwin->data)    ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_SincInterpolate, 9, sizeof(numSamplesOut),     (void*) &numSamplesOut             ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_SincInterpolate, numSamplesOut, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );
  XLALDestroyCLMEMVector ( CLMEMwin );
  XLALDestroyREAL8Window ( win );

  return XLAL_SUCCESS;
} // XLALSincInterpolateCLMEMTimeSeries()

static int
XLALResampFabX_Raw_OpenCL ( CLMEMVector *FxX_k,
                            const CLMEMVector *TS_FFT,
                            UINT4 offset_bins,
                            UINT4 decimateFFT,
                            UINT4 size
                          )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( FxX_k != NULL, XLAL_EINVAL );
  XLAL_CHECK ( TS_FFT != NULL, XLAL_EINVAL );
  cl_int err;
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ResampFabX_Raw, 0, sizeof(FxX_k->data),  (void*) &(FxX_k->data)        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ResampFabX_Raw, 1, sizeof(TS_FFT->data), (const void*) &(TS_FFT->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ResampFabX_Raw, 2, sizeof(offset_bins),  (void*) &offset_bins          ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ResampFabX_Raw, 3, sizeof(decimateFFT),  (void*) &decimateFFT          ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_ResampFabX_Raw, 4, sizeof(size),         (void*) &size                 ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_ResampFabX_Raw, size, (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;
} // XLALResampFabX_Raw_OpenCL()

static int
XLALComputeFstatFromFaFb_OpenCL ( CLMEMVector *twoFPerDet,
                                  const CLMEMVector *Fa_k,
                                  const CLMEMVector *Fb_k,
                                  REAL4 A,
                                  REAL4 B,
                                  REAL4 C,
                                  REAL4 E,
                                  REAL4 Dinv,
                                  UINT4 size
                                )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( twoFPerDet != NULL && Fa_k != NULL && Fb_k != NULL, XLAL_EINVAL );
  XLAL_CHECK ( twoFPerDet->length >= size && Fa_k->length >= size && Fb_k->length >= size, XLAL_EINVAL);
  cl_int err;
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 0, sizeof(twoFPerDet->data), (void*) &(twoFPerDet->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 1, sizeof(Fa_k->data),       (const void*) &(Fa_k->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 2, sizeof(Fb_k->data),       (const void*) &(Fb_k->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 3, sizeof(A),                (void*) &A                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 4, sizeof(B),                (void*) &B                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 5, sizeof(C),                (void*) &C                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 6, sizeof(E),                (void*) &E                  ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 7, sizeof(Dinv),             (void*) &Dinv               ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_XLALComputeFstatFromFaFb, 8, sizeof(size),             (void*) &size               ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_XLALComputeFstatFromFaFb, size, (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;
} // XLALComputeFstatFromFaFb_OpenCL()

static int
XLALNormalizeFaFb_OpenCL ( CLMEMVector *FaX_k,
                           CLMEMVector *FbX_k,
                           REAL8 dtauX,
                           REAL8 dFreq,
                           REAL8 FreqOut0,
                           REAL8 dt_SRC,
                           UINT4 size
                         )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( FaX_k != NULL && FbX_k != NULL, XLAL_EINVAL );
  XLAL_CHECK ( FaX_k->length >= size && FbX_k->length >= size, XLAL_EINVAL );
  cl_int err;
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 0, sizeof(FaX_k->data), (void*) &(FaX_k->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 1, sizeof(FbX_k->data), (void*) &(FbX_k->data) ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 2, sizeof(dtauX),       (void*) &dtauX         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 3, sizeof(dFreq),       (void*) &dFreq         ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 4, sizeof(FreqOut0),    (void*) &FreqOut0      ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 5, sizeof(dt_SRC),      (void*) &dt_SRC        ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( err = clSetKernelArg ( kernel.kernel_NormalizeFaFb, 6, sizeof(size),        (void*) &size          ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to set KernelArg: %s", XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALExecuteKernel_OpenCL ( &kernel.kernel_NormalizeFaFb, size, (1 == 1) ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;
} // XLALNormalizeFaFb_OpenCL()

int
XLALFFTisAvavible ( CLFFTType fft )
{
  switch ( fft ) {
    case CLFFT:
#ifdef HAVE_LIBCLFFT
      return 1;
#else
      return 0;
#endif
    case ECLFFT:
#ifdef HAVE_LIBECLFFT
      return 1;
#else
      return 0;
#endif

  default:
    return 0;
  }
}

int
XLALSelectFFT_OpenCL ( FFTfuncs *fftfuncs )
{
  XLAL_CHECK ( fftfuncs !=NULL, XLAL_EINVAL );
  const char *openclFFT = getenv("OPENCLFFT");
  CLFFTType selected = NONE;
  if ( openclFFT != NULL )
    {
      if ( strcmp ( openclFFT, "ECLFFT" ) == 0 && XLALFFTisAvavible ( ECLFFT ) )
        {
          selected = ECLFFT;
        }
      else if ( strcmp ( openclFFT, "CLFFT" ) == 0 && XLALFFTisAvavible ( CLFFT ) )
        {
          selected = CLFFT;
        }
       else
        {
          XLAL_ERROR ( XLAL_EINVAL, "%s: FFT %s not found or not available",__func__, openclFFT );
        }
    }
  else
    {
      if ( XLALFFTisAvavible (CLFFT) && selected == NONE )
        {
          selected = CLFFT;
        }
      if (XLALFFTisAvavible (ECLFFT) && ( selected == NONE || openclObjLocal->vendorID == 0x10DE ) )
        {
          selected = ECLFFT;
        }
    }
  if ( selected == ECLFFT )
    {
      XLALPrintInfo( "INFO: %s: selected ECLFFT\n", __func__ );
#ifdef HAVE_LIBECLFFT
      fftfuncs->initialfft_func = XLALInititialECLFFT_Opencl;
      fftfuncs->computefft_func = XLALComputeECLFFT_OpenCL;
      fftfuncs->fftplan_destroy_funnc = XLALDestroyECLFFTPlan;
#endif
    }
  else if ( selected == CLFFT )
    {
      XLALPrintInfo( "INFO: %s: selected CLFFT\n", __func__ );
#ifdef HAVE_LIBCLFFT
      fftfuncs->initialfft_func = XLALInititialFFT_Opencl;
      fftfuncs->computefft_func = XLALComputeFFT_OpenCL;
      fftfuncs->fftplan_destroy_funnc = XLALDestroyCLFFTPlan;
#endif
    }
  else
   {
     XLAL_ERROR ( XLAL_EINVAL, "%s: No available OpenCL FFT found",__func__ );
   }

  return XLAL_SUCCESS;
}

#ifdef HAVE_LIBCLFFT
static int
XLALComputeFFT_OpenCL ( void *plan,
                        CLMEMVector *in,
                        CLMEMVector *out
                      )
{
  XLAL_CHECK ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  XLAL_CHECK ( in != NULL && plan != NULL, XLAL_EINVAL );
  cl_int err;
  clfftPlanHandle *fftplan = (clfftPlanHandle*) plan;
  clfftResultLocation placeness;
  XLAL_CHECK ( ( err = clfftGetResultLocation ( (*fftplan) ,&placeness ) ) == CL_SUCCESS, XLAL_EFUNC, "clfftGetResultLocation failed: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK ( ( out == NULL || out == in ) && placeness == CLFFT_INPLACE , XLAL_EINVAL );

  /* Execute the plan. */
  err = clfftEnqueueTransform ( (*fftplan), CLFFT_FORWARD, 1, &(openclObjLocal->queue), 0, NULL, NULL, &(in->data), NULL, NULL );
  XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Processing FFT",  XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( XLALOpenCLFinish ( "End of FFT" ) == XLAL_SUCCESS, XLAL_EFUNC );

  return XLAL_SUCCESS;
} // XLALComputeFFT_OpenCL()

void *
XLALInititialFFT_Opencl ( UINT4 numSamplesFFT )
{
  XLAL_CHECK_NULL ( openclObjLocal->isInitialized, XLAL_EFUNC, "The OpenCL Object is not initialized!" );
  /* Setup clFFT. */
  clfftSetupData fftSetup;
  clfftStatus err = clfftInitSetupData ( &fftSetup );
  XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "FFT: clfftInitSetupData" ,  XLALGetOpenCLErrorString ( err ) );
  err = clfftSetup ( &fftSetup );
  XLAL_CHECK_NULL ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "FFT: clfftSetup" ,  XLALGetOpenCLErrorString ( err ) );

  clfftPlanHandle *fftplan;
  XLAL_CHECK_NULL ( (fftplan = XLALCalloc ( 1, sizeof(clfftPlanHandle)) ) != NULL, XLAL_ENOMEM );
  clfftDim dim = CLFFT_1D;
  size_t clLengths[1] = {numSamplesFFT};

  /* Create a default plan for a complex FFT. */
  XLAL_CHECK_NULL ( ( err = clfftCreateDefaultPlan ( fftplan, openclObjLocal->context, dim, clLengths ) ) == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s", "setup FFT", XLALGetOpenCLErrorString ( err ) );

  /* Set plan parameters. */
  err = clfftSetPlanPrecision ( *fftplan, CLFFT_SINGLE_FAST );
  if ( err != CL_SUCCESS )
    {
      if ( err == CLFFT_NOTIMPLEMENTED )
        {
          XLAL_CHECK_NULL ( ( err = clfftSetPlanPrecision ( *fftplan, CLFFT_SINGLE )) == CL_SUCCESS, XLAL_EFUNC, "clfftSetPlanPrecision failed: %s", XLALGetOpenCLErrorString ( err ) );
          XLALPrintInfo ( "Have to use CLFFT_SINGLE, because CLFFT_SINGLE_FAST is not implemented\n");
        }
      else
        {
          XLAL_ERROR_NULL ( XLAL_EFUNC, "clfftSetPlanPrecision failed: %s", XLALGetOpenCLErrorString ( err ) );
        }
    }
  XLAL_CHECK_NULL ( ( err = clfftSetLayout ( *fftplan, CLFFT_COMPLEX_INTERLEAVED, CLFFT_COMPLEX_INTERLEAVED )) == CL_SUCCESS, XLAL_EFUNC, "clfftSetLayout failed: %s", XLALGetOpenCLErrorString ( err ) );
  XLAL_CHECK_NULL ( ( err = clfftSetResultLocation ( *fftplan, CLFFT_INPLACE )) == CL_SUCCESS, XLAL_EFUNC, "clfftSetResultLocation failed: %s", XLALGetOpenCLErrorString ( err ) );
  /* Bake the plan. */
  XLAL_CHECK_NULL ( ( err = clfftBakePlan ( *fftplan, 1, &(openclObjLocal->queue), NULL, NULL )) == CL_SUCCESS, XLAL_EFUNC, "clfftBakePlan failed: %s", XLALGetOpenCLErrorString ( err ) );

  return fftplan;
} // XLALInititialFFT_Opencl()

static void
XLALDestroyCLFFTPlan ( void *plan )
{
  if ( !plan ) {
    return;
  }

  cl_int err;
  clfftPlanHandle *fftplan = (clfftPlanHandle*) plan;
  XLAL_CHECK_VOID ( ( err = clfftDestroyPlan( fftplan ) ) == CL_SUCCESS, XLAL_EFUNC, "Failed to release MemObject: %s", XLALGetOpenCLErrorString ( err ) );

  XLALFree ( fftplan );

  return;
} // XLALDestroyCLFFTPlan()
#endif
#ifdef HAVE_LIBECLFFT
static int
XLALComputeECLFFT_OpenCL ( void *plan,
                        CLMEMVector *in,
                        CLMEMVector *out
                      )
{
  XLAL_CHECK ( in != NULL && plan != NULL, XLAL_EINVAL );
  cl_int err;
  clFFT_Plan *fftplan = (clFFT_Plan*) plan;
  if ( out == NULL ) {
      out = in;
  }
  err = clFFT_ExecuteInterleaved ( openclObjLocal->queue, *fftplan, 1, clFFT_Forward, in->data, in->data,  0, NULL, NULL );
  XLAL_CHECK ( ( err == CL_SUCCESS ), XLAL_EFUNC, "%s failed: %s",  "Processing FFT",  XLALGetOpenCLErrorString ( err ) );

  XLAL_CHECK ( ( err =  clFinish ( openclObjLocal->queue ) ) == CL_SUCCESS, XLAL_EFUNC, "%s failed: %s",  "End of FFT",  XLALGetOpenCLErrorString ( err ) );

  return XLAL_SUCCESS;
} // XLALComputeFFT_OpenCL()

void *
XLALInititialECLFFT_Opencl ( UINT4 numSamplesFFT )
{
  clFFT_Plan *fftplan;
  XLAL_CHECK_NULL ( ( fftplan = XLALCalloc ( 1, sizeof(*fftplan) ) ) != NULL, XLAL_ENOMEM );
  cl_int err;
  clFFT_Dim3 sampleSpace = {numSamplesFFT, 1, 1};
  *fftplan = clFFT_CreatePlanAdv ( openclObjLocal->context, sampleSpace, clFFT_1D, clFFT_InterleavedComplexFormat, clFFT_TaylorLUT, &err );
  XLAL_CHECK_NULL ( err == CL_SUCCESS, XLAL_EFUNC, "setup FFT failed: %s",  XLALGetOpenCLErrorString ( err ) );

  return fftplan;
} // XLALInititialFFT_Opencl()

static void
XLALDestroyECLFFTPlan ( void *plan )
{
  if ( !plan ) {
    return;
  }

  clFFT_Plan *fftplan = (clFFT_Plan*) plan;
  clFFT_DestroyPlan( *fftplan );

  XLALFree ( fftplan );

  return;
} // XLALDestroyCLFFTPlan()
#endif
