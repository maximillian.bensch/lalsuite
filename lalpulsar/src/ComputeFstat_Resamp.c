//
// Copyright (C) 2009, 2014--2015 Reinhard Prix
// Copyright (C) 2012--2015 Karl Wette
// Copyright (C) 2009 Chris Messenger, Pinkesh Patel, Xavier Siemens, Holger Pletsch
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with with program; see the file COPYING. If not, write to the
// Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA
//

#include <stdlib.h>
#include <stdio.h>

#include "ComputeFstat_Resamp.h"

#include <lal/LogPrintf.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>

// ========== Resamp internals ==========

// ----- local macros ----------
#define MYMAX(x,y) ( (x) > (y) ? (x) : (y) )
#define MYMIN(x,y) ( (x) < (y) ? (x) : (y) )

// ----- local constants

// ----- local types ----------

static char FstatTimingResampHelp[] =
  "%%%% ----- Resampling-specific timing model -----\n"
  "%%%% NsampFFT0:      original number of FFT samples (not yet rounded up to power-of-two)\n"
  "%%%% NsampFFT:       actual number of FFT samples (rounded to power-of-two if optArgs->resampFFTPowerOf2 == true)\n"
  "%%%% R:              (internal) frequency resolution in natural units: df_internal = R / T_FFT\n"
  "%%%%\n"
  "%%%% tau0_Fbin:      timing coefficient for all contributions scaling with output frequency-bins\n"
  "%%%% tau0_spin:      timing coefficient for spindown-correction\n"
  "%%%% tau0_FFT:       timing coefficient for FFT-time\n"
  "%%%% tau0_bary:      timing coefficient for barycentering\n"
  "%%%%\n"
  "%%%% Resampling F-statistic timing model:\n"
  "%%%% tauF_core       = tau0_Fbin + (NsampFFT/NFbin) * ( R * tau0_spin + 5 * log2(NsampFFT) * tau0_FFT )\n"
  "%%%% tauF_buffer     = R * NsampFFT * tau0_bary / NFbin\n"
  "%%%%"
  "";
// ----- local prototypes ----------

int XLALSetupFstatResamp ( void **method_data, FstatCommon *common, FstatMethodFuncs* funcs, MultiSFTVector *multiSFTs, const FstatOptionalArgs *optArgs );

int
XLALSetupFstatResampGeneric ( FstatCommon *common, UINT4 numSamplesMax_SRC, ResampMethodData *resamp, FstatMethodFuncs *funcs );
int
XLALBarycentricResampleMultiCOMPLEX8TimeSeries_Generic ( ResampMethodData *resamp,
                                                         const FstatCommon *common,
                                                         MultiSSBtimes *multiSRCtimes
                                                       );
#ifdef LALPULSAR_OPENCL_ENABLED
int
XLALSetupFstatResampOpenCL ( FstatCommon *common, UINT4 numSamplesMax_SRC, ResampMethodData *resamp, FstatMethodFuncs *funcs );
int
XLALBarycentricResampleMultiCOMPLEX8TimeSeries_OPENCL ( ResampMethodData *resamp,
                                                        const FstatCommon *common,
                                                        MultiSSBtimes *multiSRCtimes
                                                        );
#endif
// ==================== function definitions ====================
// ---------- internal functions ----------
int
XLALSetupFstatResamp ( void **method_data,
                       FstatCommon *common,
                       FstatMethodFuncs* funcs,
                       MultiSFTVector *multiSFTs,
                       const FstatOptionalArgs *optArgs
                     )
{
  // Check input
  XLAL_CHECK ( method_data != NULL, XLAL_EFAULT );
  XLAL_CHECK ( common != NULL, XLAL_EFAULT );
  XLAL_CHECK ( funcs != NULL, XLAL_EFAULT );
  XLAL_CHECK ( multiSFTs != NULL, XLAL_EFAULT );
  XLAL_CHECK ( optArgs != NULL, XLAL_EFAULT );
  XLAL_CHECK ( optArgs->FstatMethod != FMETHOD_RESAMP_OPENCL || optArgs->resampFFTPowerOf2, XLAL_EINVAL, "ResampOpenCL only supports Power of 2" );

  // Allocate method data
  ResampMethodData *resamp = *method_data = XLALCalloc( 1, sizeof(*resamp) );
  XLAL_CHECK( resamp != NULL, XLAL_ENOMEM );

  resamp->Dterms = optArgs->Dterms;

  // Extra band needed for resampling: Hamming-windowed sinc used for interpolation has a transition bandwith of
  // TB=(4/L)*fSamp, where L=2*Dterms+1 is the window-length, and here fSamp=Band (i.e. the full SFT frequency band)
  // However, we're only interested in the physical band and we'll be throwing away all bins outside of this.
  // This implies that we're only affected by *half* the transition band TB/2 on either side, as the other half of TB is outside of the band of interest
  // (and will actually get aliased, i.e. the region [-fNy - TB/2, -fNy] overlaps with [fNy-TB/2,fNy] and vice-versa: [fNy,fNy+TB/2] overlaps with [-fNy,-fNy+TB/2])
  // ==> therefore we only need to add an extra TB/2 on each side to be able to safely avoid the transition-band effects
  REAL8 f0 = multiSFTs->data[0]->data[0].f0;
  REAL8 dFreq = multiSFTs->data[0]->data[0].deltaF;
  REAL8 Band = multiSFTs->data[0]->data[0].data->length * dFreq;
  REAL8 extraBand = 2.0  / ( 2 * optArgs->Dterms + 1 ) * Band;
  XLAL_CHECK ( XLALMultiSFTVectorResizeBand ( multiSFTs, f0 - extraBand, Band + 2 * extraBand ) == XLAL_SUCCESS, XLAL_EFUNC );
  // Convert SFTs into heterodyned complex timeseries [in detector frame]
  XLAL_CHECK ( (resamp->multiTimeSeries_DET = XLALMultiSFTVectorToCOMPLEX8TimeSeries ( multiSFTs )) != NULL, XLAL_EFUNC );

  XLALDestroyMultiSFTVector ( multiSFTs );	// don't need them SFTs any more ...

  UINT4 numDetectors = resamp->multiTimeSeries_DET->length;
  REAL8 dt_DET       = resamp->multiTimeSeries_DET->data[0]->deltaT;
  REAL8 fHet         = resamp->multiTimeSeries_DET->data[0]->f0;
  REAL8 Tsft         = common->multiTimestamps->data[0]->deltaT;

  // determine resampled timeseries parameters
  REAL8 TspanFFT = 1.0 / common->dFreq;

  // check that TspanFFT >= TspanX for all detectors X, otherwise increase TspanFFT by an integer factor 'decimateFFT' such that this is true
  REAL8 TspanXMax = 0;
  for ( UINT4 X = 0; X < numDetectors; X ++ )
    {
      UINT4 numSamples_DETX = resamp->multiTimeSeries_DET->data[X]->data->length;
      REAL8 TspanX = numSamples_DETX * dt_DET;
      TspanXMax = fmax ( TspanXMax, TspanX );
    }
  UINT4 decimateFFT = (UINT4)ceil ( TspanXMax / TspanFFT );	// larger than 1 means we need to artificially increase dFreqFFT by 'decimateFFT'
  if ( decimateFFT > 1 ) {
    XLALPrintWarning ("WARNING: Frequency spacing larger than 1/Tspan, we'll internally decimate FFT frequency bins by a factor of %" LAL_UINT4_FORMAT "\n", decimateFFT );
  }
  TspanFFT *= decimateFFT;
  resamp->decimateFFT = decimateFFT;

  UINT4 numSamplesFFT0 = (UINT4) ceil ( TspanFFT / dt_DET );      // we use ceil() so that we artificially widen the band rather than reduce it
  UINT4 numSamplesFFT = 0;
  if ( optArgs->resampFFTPowerOf2 ) {
    numSamplesFFT = (UINT4) pow ( 2, ceil ( log2 ( numSamplesFFT0 ) ) );  // round numSamplesFFT up to next power of 2 for most effiecient FFT
  } else {
    numSamplesFFT = (UINT4) 2 * ceil ( numSamplesFFT0 / 2 );	// always ensure numSamplesFFT is even
  }

  REAL8 dt_SRC = TspanFFT / numSamplesFFT;			// adjust sampling rate to allow achieving exact requested dFreq=1/TspanFFT !

  resamp->numSamplesFFT = numSamplesFFT;
  // ----- allocate buffer Memory ----------

  // header for SRC-frame resampled timeseries buffer
  XLAL_CHECK ( (resamp->multiTimeSeries_SRC_a = XLALCalloc ( 1, sizeof(MultiCOMPLEX8TimeSeries)) ) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (resamp->multiTimeSeries_SRC_a->data = XLALCalloc ( numDetectors, sizeof(COMPLEX8TimeSeries) )) != NULL, XLAL_ENOMEM );
  resamp->multiTimeSeries_SRC_a->length = numDetectors;

  XLAL_CHECK ( (resamp->multiTimeSeries_SRC_b = XLALCalloc ( 1, sizeof(MultiCOMPLEX8TimeSeries)) ) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (resamp->multiTimeSeries_SRC_b->data = XLALCalloc ( numDetectors, sizeof(COMPLEX8TimeSeries) )) != NULL, XLAL_ENOMEM );
  resamp->multiTimeSeries_SRC_b->length = numDetectors;

  LIGOTimeGPS XLAL_INIT_DECL(epoch0);	// will be set to corresponding SRC-frame epoch when barycentering
  UINT4 numSamplesMax_SRC = 0;
  for ( UINT4 X = 0; X < numDetectors; X ++ )
    {
      // ----- check input consistency ----------
      REAL8 dt_DETX = resamp->multiTimeSeries_DET->data[X]->deltaT;
      XLAL_CHECK ( dt_DET == dt_DETX, XLAL_EINVAL, "Input timeseries must have identical 'deltaT(X=%d)' (%.16g != %.16g)\n", X, dt_DET, dt_DETX);

      REAL8 fHetX = resamp->multiTimeSeries_DET->data[X]->f0;
      XLAL_CHECK ( fabs( fHet - fHetX ) < LAL_REAL8_EPS * fHet, XLAL_EINVAL, "Input timeseries must have identical heterodyning frequency 'f0(X=%d)' (%.16g != %.16g)\n", X, fHet, fHetX );

      REAL8 TsftX = common->multiTimestamps->data[X]->deltaT;
      XLAL_CHECK ( Tsft == TsftX, XLAL_EINVAL, "Input timestamps must have identical stepsize 'Tsft(X=%d)' (%.16g != %.16g)\n", X, Tsft, TsftX );

      // ----- prepare Memory fo SRC-frame timeseries and AM coefficients
      const char *nameX = resamp->multiTimeSeries_DET->data[X]->name;
      UINT4 numSamples_DETX = resamp->multiTimeSeries_DET->data[X]->data->length;
      UINT4 numSamples_SRCX = (UINT4)ceil ( numSamples_DETX * dt_DET / dt_SRC );

      XLAL_CHECK ( (resamp->multiTimeSeries_SRC_a->data[X] = XLALCreateCOMPLEX8TimeSeries ( nameX, &epoch0, fHet, dt_SRC, &lalDimensionlessUnit, numSamples_SRCX )) != NULL, XLAL_EFUNC );
      XLAL_CHECK ( (resamp->multiTimeSeries_SRC_b->data[X] = XLALCreateCOMPLEX8TimeSeries ( nameX, &epoch0, fHet, dt_SRC, &lalDimensionlessUnit, numSamples_SRCX )) != NULL, XLAL_EFUNC );

      numSamplesMax_SRC = MYMAX ( numSamplesMax_SRC, numSamples_SRCX );
    } // for X < numDetectors

  XLAL_CHECK ( numSamplesFFT >= numSamplesMax_SRC, XLAL_EFAILED, "[numSamplesFFT = %d] < [numSamplesMax_SRC = %d]\n", numSamplesFFT, numSamplesMax_SRC );

  // ---- setup method specific intital function
  switch ( optArgs->FstatMethod ) {

    case FMETHOD_RESAMP_OPENCL:
#ifdef LALPULSAR_OPENCL_ENABLED
      XLAL_CHECK ( XLALSetupFstatResampOpenCL ( common, numSamplesMax_SRC, resamp, funcs ) == XLAL_SUCCESS, XLAL_EFUNC );
#endif
      break;

    case FMETHOD_RESAMP_GENERIC:
      XLAL_CHECK ( XLALSetupFstatResampGeneric ( common, numSamplesMax_SRC, resamp, funcs ) == XLAL_SUCCESS, XLAL_EFUNC );
      break;

  default:
    XLAL_ERROR ( XLAL_EFAILED, "Missing switch case for optArgs.FstatMethod='%d'\n", optArgs->FstatMethod );
    }
  resamp->FstatMethod = optArgs->FstatMethod;

  // turn on timing collection if requested
  resamp->collectTiming = optArgs->collectTiming;

  // initialize struct for collecting timing data, store invariant 'meta' quantities about this setup
  if ( resamp->collectTiming )
    {
      XLAL_INIT_MEM ( resamp->timingGeneric );
      resamp->timingGeneric.Ndet = numDetectors;

      XLAL_INIT_MEM ( resamp->timingResamp );
      resamp->timingResamp.Resolution = TspanXMax / TspanFFT;
      resamp->timingResamp.NsampFFT0  = numSamplesFFT0;
      resamp->timingResamp.NsampFFT   = numSamplesFFT;
    }

  return XLAL_SUCCESS;

} // XLALSetupFstatResamp()

int
XLALExtractResampledTimeseries_intern ( MultiCOMPLEX8TimeSeries **multiTimeSeries_SRC_a, MultiCOMPLEX8TimeSeries **multiTimeSeries_SRC_b, const void* method_data )
{
  XLAL_CHECK ( method_data != NULL, XLAL_EINVAL );
  XLAL_CHECK ( ( multiTimeSeries_SRC_a != NULL ) && ( multiTimeSeries_SRC_b != NULL ) , XLAL_EINVAL );
  XLAL_CHECK ( method_data != NULL, XLAL_EINVAL );

  const ResampMethodData *resamp = (const ResampMethodData *) method_data;
  *multiTimeSeries_SRC_a = resamp->multiTimeSeries_SRC_a;
  *multiTimeSeries_SRC_b = resamp->multiTimeSeries_SRC_b;

  return XLAL_SUCCESS;

} // XLALExtractResampledTimeseries_intern()

int
XLALGetFstatTiming_Resamp ( const void *method_data, FstatTimingGeneric *timingGeneric, FstatTimingModel *timingModel )
{
  XLAL_CHECK ( method_data != NULL, XLAL_EINVAL );
  XLAL_CHECK ( timingGeneric != NULL, XLAL_EINVAL );
  XLAL_CHECK ( timingModel != NULL, XLAL_EINVAL );

  const ResampMethodData *resamp = (const ResampMethodData*) method_data;
  XLAL_CHECK ( resamp != NULL, XLAL_EINVAL );

  (*timingGeneric) = resamp->timingGeneric; // struct-copy generic timing measurements

  const FstatTimingResamp *tiRS = &(resamp->timingResamp);

  // return method-specific timing model values
  XLAL_INIT_MEM( (*timingModel) );

  UINT4 i = 0;
  timingModel->names[i]  = "NsampFFT0";
  timingModel->values[i] = tiRS->NsampFFT0;

  i++;
  timingModel->names[i]  = "NsampFFT";
  timingModel->values[i] = tiRS->NsampFFT;

  i++;
  timingModel->names[i]  = "Resolution";
  timingModel->values[i] = tiRS->Resolution;

  i++;
  timingModel->names[i]  = "tau0_Fbin";
  timingModel->values[i] = tiRS->tau0_Fbin;

  i++;
  timingModel->names[i]  = "tau0_spin";
  timingModel->values[i] = tiRS->tau0_spin;

  i++;
  timingModel->names[i]  = "tau0_FFT";
  timingModel->values[i] = tiRS->tau0_FFT;

  i++;
  timingModel->names[i]  = "tau0_bary";
  timingModel->values[i] = tiRS->tau0_bary;

  timingModel->numVariables = i+1;
  timingModel->help      = FstatTimingResampHelp;

  return XLAL_SUCCESS;
} // XLALGetFstatTiming_Resamp()

///
/// Performs barycentric resampling on a multi-detector timeseries, updates resampling buffer with results
///
/// NOTE Buffering: this function does check
/// 1) whether the previously-buffered solution can be completely reused (same sky-position and binary parameters), or
/// 2) if at least sky-dependent quantities can be re-used (antenna-patterns + timings) in case only binary parameters changed
///
int
XLALBarycentricResampleMultiCOMPLEX8TimeSeries ( ResampMethodData *resamp,		// [in/out] resampling input and buffer (to store resampling TS)
                                                 const PulsarDopplerParams *thisPoint,	// [in] current skypoint and reftime
                                                 const FstatCommon *common		// [in] various input quantities and parameters used here
                                                 )
{
  // check input sanity
  XLAL_CHECK ( thisPoint != NULL, XLAL_EINVAL );
  XLAL_CHECK ( common != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_DET != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_a != NULL, XLAL_EINVAL );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_b != NULL, XLAL_EINVAL );

  UINT4 numDetectors = resamp->multiTimeSeries_DET->length;
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_a->length == numDetectors, XLAL_EINVAL, "Inconsistent number of detectors tsDET(%d) != tsSRC(%d)\n", numDetectors, resamp->multiTimeSeries_SRC_a->length );
  XLAL_CHECK ( resamp->multiTimeSeries_SRC_b->length == numDetectors, XLAL_EINVAL, "Inconsistent number of detectors tsDET(%d) != tsSRC(%d)\n", numDetectors, resamp->multiTimeSeries_SRC_b->length );

  // ============================== BEGIN: handle buffering =============================
  BOOLEAN same_skypos = (resamp->prev_doppler.Alpha == thisPoint->Alpha) && (resamp->prev_doppler.Delta == thisPoint->Delta);
  BOOLEAN same_refTime = ( XLALGPSDiff ( &(resamp->prev_doppler.refTime), &(thisPoint->refTime) ) == 0 );
  BOOLEAN same_binary = \
    (resamp->prev_doppler.asini == thisPoint->asini) &&
    (resamp->prev_doppler.period == thisPoint->period) &&
    (resamp->prev_doppler.ecc == thisPoint->ecc) &&
    (XLALGPSDiff( &(resamp->prev_doppler.tp), &(thisPoint->tp) ) == 0 ) &&
    (resamp->prev_doppler.argp == thisPoint->argp);

  Timings_t *Tau = &(resamp->timingResamp.Tau);
  REAL8 tic = 0, toc = 0;
  BOOLEAN collectTiming = resamp->collectTiming;

  // if same sky-position *and* same binary, we can simply return as there's nothing to be done here
  if ( same_skypos && same_refTime && same_binary ) {
    Tau->BufferRecomputed = 0;
    return XLAL_SUCCESS;
  }
  // else: keep track of 'buffer miss', ie we need to recompute the buffer
  Tau->BufferRecomputed = 1;
  resamp->timingGeneric.NBufferMisses ++;
  if ( collectTiming ) {
    tic = XLALGetCPUTime();
  }

  MultiSSBtimes *multiSRCtimes = NULL;

  // only if different sky-position: re-compute antenna-patterns and SSB timings, re-use from buffer otherwise
  if ( ! ( same_skypos && same_refTime ) )
    {
      SkyPosition skypos;
      skypos.system = COORDINATESYSTEM_EQUATORIAL;
      skypos.longitude = thisPoint->Alpha;
      skypos.latitude  = thisPoint->Delta;

      XLALDestroyMultiAMCoeffs ( resamp->multiAMcoef );
      XLAL_CHECK ( (resamp->multiAMcoef = XLALComputeMultiAMCoeffs ( common->multiDetectorStates, common->multiNoiseWeights, skypos )) != NULL, XLAL_EFUNC );
      resamp->Mmunu = resamp->multiAMcoef->Mmunu;
      for ( UINT4 X = 0; X < numDetectors; X ++ )
        {
          resamp->MmunuX[X].Ad = resamp->multiAMcoef->data[X]->A;
          resamp->MmunuX[X].Bd = resamp->multiAMcoef->data[X]->B;
          resamp->MmunuX[X].Cd = resamp->multiAMcoef->data[X]->C;
          resamp->MmunuX[X].Ed = 0;
          resamp->MmunuX[X].Dd = resamp->multiAMcoef->data[X]->D;
        }

      XLALDestroyMultiSSBtimes ( resamp->multiSSBtimes );
      XLAL_CHECK ( (resamp->multiSSBtimes = XLALGetMultiSSBtimes ( common->multiDetectorStates, skypos, thisPoint->refTime, common->SSBprec )) != NULL, XLAL_EFUNC );

    } // if cannot re-use buffered solution ie if !(same_skypos && same_binary)

  if ( thisPoint->asini > 0 ) { // binary case
    XLAL_CHECK ( XLALAddMultiBinaryTimes ( &resamp->multiBinaryTimes, resamp->multiSSBtimes, thisPoint ) == XLAL_SUCCESS, XLAL_EFUNC );
    multiSRCtimes = resamp->multiBinaryTimes;
  } else { // isolated case
    multiSRCtimes = resamp->multiSSBtimes;
  }

  // record barycenter parameters in order to allow re-usal of this result ('buffering')
  resamp->prev_doppler = (*thisPoint);

  // ---- setup method specific intital function
  switch ( resamp->FstatMethod ) {

    case FMETHOD_RESAMP_OPENCL:
#ifdef LALPULSAR_OPENCL_ENABLED
      XLAL_CHECK ( XLALBarycentricResampleMultiCOMPLEX8TimeSeries_OPENCL ( resamp, common, multiSRCtimes ) == XLAL_SUCCESS, XLAL_EFUNC );
#endif
      break;

    case FMETHOD_RESAMP_GENERIC:
      XLAL_CHECK ( XLALBarycentricResampleMultiCOMPLEX8TimeSeries_Generic ( resamp, common, multiSRCtimes ) == XLAL_SUCCESS, XLAL_EFUNC );
      break;

  default:
    XLAL_ERROR ( XLAL_EFAILED, "Missing switch case for optArgs.FstatMethod='%d'\n", resamp->FstatMethod );
    }

  if ( collectTiming ) {
    toc = XLALGetCPUTime();
    Tau->Bary = (toc-tic);
  }

  return XLAL_SUCCESS;
} // XLALBarycentricResampleMultiCOMPLEX8TimeSeries
