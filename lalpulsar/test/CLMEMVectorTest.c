/*
*  Copyright (C) 2018 Maximillian Bensch
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with with program; see the file COPYING. If not, write to the
*  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
*  MA  02111-1307  USA
*/
#include <math.h>
#include <stdlib.h>

#include <config.h>

#include <lal/LALVCSInfo.h>
#include <lal/LALConstants.h>
#include <lal/XLALError.h>
#include <lal/AVFactories.h>
#include <lal/LogPrintf.h>	 // for timing function XLALGetCPUTime()
#include <lal/UserInput.h>

#include <lal/VectorMath.h>
#include <lal/OpenCLutils.h>

// ---------- Macros ----------
#define CONCATCL(a,b) "XLAL" #a "CLMEMVector" #b
#define frand() (rand() / (REAL4)RAND_MAX)
#define Relerr(dx,x) (fabsf(x)>0 ? fabsf((dx)/(x)) : fabsf(dx) )
#define Relerrd(dx,x) (fabs(x)>0 ? fabs((dx)/(x)) : fabs(dx) )
#define cRelerr(dx,x) (cabsf(x)>0 ? cabsf((dx)/(x)) : fabsf(dx) )

#define TESTBENCH_VECTORMATH_SS2S(name,in1,in2,in1_CL,in2_CL)                         \
  {                                                                     \
    const char* XLAL##name##CLMEMVectorREAL4_name = CONCATCL(name,REAL4); \
    XLAL_CHECK ( XLALVector##name##REAL4( xOutRef, in1, in2, Ntrials ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    tic = XLALGetCPUTime();                                             \
    for (UINT4 l=0; l < Nruns; l ++ ) {                                 \
      XLAL_CHECK ( XLAL##name##CLMEMVectorREAL4( xOut_CL, in1_CL, in2_CL, Ntrials, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    }                                                                   \
    toc = XLALGetCPUTime();                                             \
    XLAL_CHECK (  XLALGetVectorFromCLMEMVector ( xOut, xOut_CL, Ntrials, sizeof(xOut[0]), (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );\
    maxErr = maxRelerr = 0;                                             \
    for ( UINT4 i = 0; i < Ntrials; i ++ )                              \
    {                                                                   \
      REAL4 err = fabsf ( xOut[i] - xOutRef[i] );                      \
      REAL4 relerr = Relerr ( err, xOutRef[i] );                       \
      maxErr    = fmaxf ( err, maxErr );                                \
      maxRelerr = fmaxf ( relerr, maxRelerr );                          \
    }                                                                   \
    XLALPrintInfo ( "%-32s: %4.0f Mops/sec [maxErr = %7.2g (tol=%7.2g), maxRelerr = %7.2g (tol=%7.2g)]\n", \
                    XLAL##name##CLMEMVectorREAL4_name, (REAL8)Ntrials * Nruns / (toc - tic)/1e6, maxErr, (abstol), maxRelerr, (reltol) ); \
    XLAL_CHECK ( (maxErr <= (abstol)), XLAL_ETOL, "%s: absolute error (%g) exceeds tolerance (%g)\n", #name "REAL4", maxErr, abstol ); \
    XLAL_CHECK ( (maxRelerr <= (reltol)), XLAL_ETOL, "%s: relative error (%g) exceeds tolerance (%g)\n", #name "REAL4", maxRelerr, reltol ); \
  }

#define TESTBENCH_VECTORMATH_DD2D(name,in1,in2, in1_CL, in2_CL)                         \
  {                                                                     \
    const char* XLAL##name##CLMEMVectorREAL8_name = CONCATCL(name,REAL8); \
    XLAL_CHECK ( XLALVector##name##REAL8( xOutRefD, in1, in2, Ntrials ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    tic = XLALGetCPUTime();                                             \
    for (UINT4 l=0; l < Nruns; l ++ ) {                                 \
      XLAL_CHECK ( XLAL##name##CLMEMVectorREAL8( xOutD_CL, in1_CL, in2_CL, Ntrials, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    }                                                                   \
    toc = XLALGetCPUTime();                                             \
    XLAL_CHECK (  XLALGetVectorFromCLMEMVector ( xOutD, xOutD_CL, Ntrials, sizeof(xOutD[0]), (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );\
    maxErr = maxRelerr = 0;                                             \
    for ( UINT4 i = 0; i < Ntrials; i ++ )                              \
    {                                                                   \
      REAL8 err = fabs ( xOutD[i] - xOutRefD[i] );                      \
      REAL8 relerr = Relerrd ( err, xOutRefD[i] );                       \
      maxErr    = fmax ( err, maxErr );                                \
      maxRelerr = fmax ( relerr, maxRelerr );                          \
    }                                                                   \
    XLALPrintInfo ( "%-32s: %4.0f Mops/sec [maxErr = %7.2g (tol=%7.2g), maxRelerr = %7.2g (tol=%7.2g)]\n", \
                    XLAL##name##CLMEMVectorREAL8_name, (REAL8)Ntrials * Nruns / (toc - tic)/1e6, maxErr, (abstol), maxRelerr, (reltol) ); \
    XLAL_CHECK ( (maxErr <= (abstol)), XLAL_ETOL, "%s: absolute error (%g) exceeds tolerance (%g)\n", #name "REAL8", maxErr, abstol ); \
    XLAL_CHECK ( (maxRelerr <= (reltol)), XLAL_ETOL, "%s: relative error (%g) exceeds tolerance (%g)\n", #name "REAL8", maxRelerr, reltol ); \
  }

#define TESTBENCH_VECTORMATH_CC2C(name,in1,in2,in1_CL, in2_CL)                         \
  {                                                                     \
    const char* XLAL##name##CLMEMVectorCOMPLEX8_name = CONCATCL(name,COMPLEX8); \
    XLAL_CHECK ( XLALVector##name##COMPLEX8( xOutRefC, in1, in2, Ntrials ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    tic = XLALGetCPUTime();                                             \
    for (UINT4 l=0; l < Nruns; l ++ ) {                                 \
      XLAL_CHECK ( XLAL##name##CLMEMVectorCOMPLEX8( xOutC_CL, in1_CL, in2_CL, Ntrials, (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC ); \
    }                                                                   \
    toc = XLALGetCPUTime();                                             \
    XLAL_CHECK (  XLALGetVectorFromCLMEMVector ( xOutC, xOutC_CL, Ntrials, sizeof(xOutC[0]), (1==1) ) == XLAL_SUCCESS, XLAL_EFUNC );\
    maxErr = maxRelerr = 0;                                             \
    for ( UINT4 i = 0; i < Ntrials; i ++ )                              \
    {                                                                   \
      REAL4 err = cabsf ( xOutC[i] - xOutRefC[i] );                      \
      REAL4 relerr = cRelerr ( err, xOutRefC[i] );                       \
      maxErr    = fmaxf ( err, maxErr );                                \
      maxRelerr = fmaxf ( relerr, maxRelerr );                          \
    }                                                                   \
    XLALPrintInfo ( "%-32s: %4.0f Mops/sec [maxErr = %7.2g (tol=%7.2g), maxRelerr = %7.2g (tol=%7.2g)]\n", \
                    XLAL##name##CLMEMVectorCOMPLEX8_name, (REAL8)Ntrials * Nruns / (toc - tic)/1e6, maxErr, (abstol), maxRelerr, (reltol) ); \
    XLAL_CHECK ( (maxErr <= (abstol)), XLAL_ETOL, "%s: absolute error (%g) exceeds tolerance (%g)\n", #name "COMPLEX8", maxErr, abstol ); \
    XLAL_CHECK ( (maxRelerr <= (reltol)), XLAL_ETOL, "%s: relative error (%g) exceeds tolerance (%g)\n", #name "COMPLEX8", maxRelerr, reltol ); \
  }

// local types
typedef struct
{
  INT4 randSeed;	/**< allow user to specify random-number seed for reproducible noise-realizations */
  INT4 Nruns;		// number of repated timing 'runs' to average over in order to improve variance of result
  INT4 inAlign;		// alignment of input vectors; default is sizeof(void*), i.e. no particular alignment
  INT4 outAlign;	// alignment of output vectors; default is sizeof(void*), i.e. no particular alignment
} UserInput_t;


// ---------- main ----------
int
main ( int argc, char *argv[] )
{
  UserInput_t XLAL_INIT_DECL(uvar_s);
  UserInput_t *uvar = &uvar_s;

  uvar->randSeed = 1;
  uvar->Nruns = 1;
  uvar->inAlign = uvar->outAlign = sizeof(void*);
  // ---------- register user-variable ----------
  XLALRegisterUvarMember(  randSeed,            INT4, 's', OPTIONAL, "Random-number seed");
  XLALRegisterUvarMember(  Nruns,               INT4, 'r', OPTIONAL, "Number of repeated timing 'runs' to average over (=improves variance)" );
  XLALRegisterUvarMember(  inAlign,             INT4, 'a', OPTIONAL, "Alignment of input vectors; default is sizeof(void*), i.e. no particular alignment" );
  XLALRegisterUvarMember(  outAlign,            INT4, 'b', OPTIONAL, "Alignment of output vectors; default is sizeof(void*), i.e. no particular alignment" );

  BOOLEAN should_exit = 0;
  XLAL_CHECK( XLALUserVarReadAllInput( &should_exit, argc, argv, lalVCSInfoList ) == XLAL_SUCCESS, XLAL_EFUNC );
  if ( should_exit ) {
    exit (1);
  }

  srand ( uvar->randSeed );
  XLAL_CHECK ( uvar->Nruns >= 1, XLAL_EDOM );
  UINT4 Nruns = (UINT4)uvar->Nruns;

  XLAL_CHECK ( XLALCreateOpenCL_OBJ ( ) != NULL, XLAL_EFUNC, "could not create OpenCL_OBJ" );

  UINT4 Ntrials = 1000000 + 7;
  REAL4VectorAligned *xIn_a, *xIn2_a, *xOut_a;
  XLAL_CHECK ( ( xIn_a   = XLALCreateREAL4VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xIn2_a  = XLALCreateREAL4VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xOut_a  = XLALCreateREAL4VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );
  REAL4VectorAligned *xOutRef_a;
  XLAL_CHECK ( (xOutRef_a  = XLALCreateREAL4VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );

  // extract aligned REAL4 vectors from these
  REAL4 *xIn      = xIn_a->data;
  REAL4 *xIn2     = xIn2_a->data;
  REAL4 *xOut     = xOut_a->data;
  REAL4 *xOutRef  = xOutRef_a->data;

  CLMEMVector *xOut_CL,*xIn2_CL, *xIn_CL;
  XLAL_CHECK ( (xOut_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL4), CL_MEM_WRITE_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xIn_CL  = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL4), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xIn2_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL4), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );

  REAL8VectorAligned *xInD_a, *xIn2D_a, *xOutD_a, *xOutRefD_a;
  XLAL_CHECK ( ( xInD_a   = XLALCreateREAL8VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xIn2D_a  = XLALCreateREAL8VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xOutD_a  = XLALCreateREAL8VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( (xOutRefD_a= XLALCreateREAL8VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );

  // extract aligned REAL8 vectors from these
  REAL8 *xInD      = xInD_a->data;
  REAL8 *xIn2D     = xIn2D_a->data;
  REAL8 *xOutD     = xOutD_a->data;
  REAL8 *xOutRefD  = xOutRefD_a->data;

  CLMEMVector *xOutD_CL,*xIn2D_CL, *xInD_CL;
  XLAL_CHECK ( (xOutD_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL8), CL_MEM_WRITE_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xInD_CL  = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xIn2D_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(REAL8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );

  COMPLEX8VectorAligned *xInC_a, *xIn2C_a, *xOutC_a, *xOutRefC_a;
  XLAL_CHECK ( ( xInC_a   = XLALCreateCOMPLEX8VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xIn2C_a  = XLALCreateCOMPLEX8VectorAligned ( Ntrials, uvar->inAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( ( xOutC_a  = XLALCreateCOMPLEX8VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );
  XLAL_CHECK ( (xOutRefC_a  = XLALCreateCOMPLEX8VectorAligned ( Ntrials, uvar->outAlign )) != NULL, XLAL_EFUNC );

  // extract aligned COMPLEX8 vectors from these
  COMPLEX8 *xInC      = xInC_a->data;
  COMPLEX8 *xIn2C     = xIn2C_a->data;
  COMPLEX8 *xOutC     = xOutC_a->data;
  COMPLEX8 *xOutRefC  = xOutRefC_a->data;

  CLMEMVector *xOutC_CL,*xIn2C_CL, *xInC_CL;
  XLAL_CHECK ( (xOutC_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(COMPLEX8), CL_MEM_WRITE_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xInC_CL  = XLALCreateCLMEMVector ( Ntrials, sizeof(COMPLEX8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );
  XLAL_CHECK ( (xIn2C_CL = XLALCreateCLMEMVector ( Ntrials, sizeof(COMPLEX8), CL_MEM_READ_ONLY )) != NULL, XLAL_ENOMEM );


  REAL8 tic, toc;
  REAL4 maxErr = 0, maxRelerr = 0;
  REAL4 abstol, reltol;

  // ==================== ADD,MUL,ROUND ====================
  for ( UINT4 i = 0; i < Ntrials; i ++ ) {
    xIn[i]  = -10000.0f + 20000.0f * frand() + 1e-6;
    xIn2[i] = -10000.0f + 20000.0f * frand() + 1e-6;
    xInD[i] = -100000.0 + 200000.0 * frand() + 1e-6;
    xIn2D[i]= -100000.0 + 200000.0 * frand() + 1e-6;
    xInC[i] = -10000.0f + 20000.0f * frand() + 1e-6 + ( -10000.0f + 20000.0f * frand() + 1e-6 ) * _Complex_I;
    xIn2C[i]= -10000.0f + 20000.0f * frand() + 1e-6 + ( -10000.0f + 20000.0f * frand() + 1e-6 ) * _Complex_I;
  } // for i < Ntrials
  abstol = 2e-7, reltol = 2e-7;

  XLALTransferVectorToCLMEMVector ( xIn_CL,  xIn,    Ntrials, sizeof(xIn[0]),   (1==0) );
  XLALTransferVectorToCLMEMVector ( xIn2_CL, xIn2,   Ntrials, sizeof(xIn2[0]),  (1==0) );
  XLALTransferVectorToCLMEMVector ( xInD_CL, xInD,   Ntrials, sizeof(xInD[0]),  (1==0) );
  XLALTransferVectorToCLMEMVector ( xIn2D_CL, xIn2D, Ntrials, sizeof(xIn2D[0]), (1==0) );
  XLALTransferVectorToCLMEMVector ( xInC_CL,  xInC,  Ntrials, sizeof(xInC[0]),  (1==0) );
  XLALTransferVectorToCLMEMVector ( xIn2C_CL, xIn2C, Ntrials, sizeof(xIn2C[0]), (1==1) );

  XLALPrintInfo ("Testing add,multiply(x,y) for x,y in (-10000, 10000]\n");
  TESTBENCH_VECTORMATH_SS2S(Add,xIn,xIn2,xIn_CL,xIn2_CL);
  TESTBENCH_VECTORMATH_SS2S(Multiply,xIn,xIn2,xIn_CL,xIn2_CL);

  TESTBENCH_VECTORMATH_DD2D(Add,xInD,xIn2D,xInD_CL,xIn2D_CL);
  TESTBENCH_VECTORMATH_DD2D(Multiply,xInD,xIn2D,xInD_CL,xIn2D_CL);

  TESTBENCH_VECTORMATH_CC2C(Add,xInC,xIn2C,xInC_CL,xIn2C_CL);
  abstol = 20; // Exceed tolerance for NVIDIA GPUs
  TESTBENCH_VECTORMATH_CC2C(Multiply,xInC,xIn2C,xInC_CL,xIn2C_CL);

  XLALPrintInfo ("\n");

  // ---------- clean up memory ----------
  XLALDestroyREAL4VectorAligned ( xIn_a );
  XLALDestroyREAL4VectorAligned ( xIn2_a );
  XLALDestroyREAL4VectorAligned ( xOut_a );
  XLALDestroyREAL4VectorAligned ( xOutRef_a );

  XLALDestroyREAL8VectorAligned ( xInD_a );
  XLALDestroyREAL8VectorAligned ( xIn2D_a );
  XLALDestroyREAL8VectorAligned ( xOutD_a );
  XLALDestroyREAL8VectorAligned ( xOutRefD_a );

  XLALDestroyCOMPLEX8VectorAligned ( xInC_a );
  XLALDestroyCOMPLEX8VectorAligned ( xIn2C_a );
  XLALDestroyCOMPLEX8VectorAligned ( xOutC_a );
  XLALDestroyCOMPLEX8VectorAligned ( xOutRefC_a );

  XLALDestroyCLMEMVector ( xInD_CL ); 
  XLALDestroyCLMEMVector ( xIn2D_CL ); 
  XLALDestroyCLMEMVector ( xOutD_CL ); 
  XLALDestroyCLMEMVector ( xIn_CL ); 
  XLALDestroyCLMEMVector ( xIn2_CL ); 
  XLALDestroyCLMEMVector ( xOut_CL ); 
  XLALDestroyCLMEMVector ( xInC_CL ); 
  XLALDestroyCLMEMVector ( xIn2C_CL ); 
  XLALDestroyCLMEMVector ( xOutC_CL ); 
  XLALDestroyOpenCL_OBJ();

  XLALDestroyUserVars();

  LALCheckMemoryLeaks();

  return XLAL_SUCCESS;

} // main()
